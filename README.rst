===========================
The ``lino-medico`` package
===========================




A hospital management application (in development);

Scope
=====

A global database system for all indexed hospital

  - Hospital's internal access for management
  - External access when needed

Users (patients) can access personal medical history via web portal

