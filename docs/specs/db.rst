.. doctest docs/specs/db.rst
.. _medico.specs.db:

=================================
Database structure of Lino Medico
=================================

This document describes the database structure.

.. contents::
  :local:


.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_medico.projects.medico1.settings.doctests')
>>> from lino.api.doctest import *


>>> analyzer.show_db_overview()
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
41 plugins: lino, staticfiles, about, printing, system, jinja, react, channels, linod, users, medico, office, xl, countries, contacts, groups, contenttypes, gfks, checkdata, cal, calview, reception, courses, orders, measurements, products, memo, excerpts, weasyprint, uploads, ledger, vat, sales, invoicing, comments, notify, search, export_excel, tinymce, appypod, sessions.
88 models:
================================= ================================== ========= =======
 Name                              Default table                      #fields   #rows
--------------------------------- ---------------------------------- --------- -------
 cal.Calendar                      cal.Calendars                      6         0
 cal.EntryRepeater                 cal.EntryRepeaterTable             17        0
 cal.Event                         cal.Events                         29        0
 cal.EventPolicy                   cal.EventPolicies                  20        0
 cal.EventType                     cal.EventTypes                     24        4
 cal.Guest                         cal.Guests                         10        0
 cal.GuestRole                     cal.GuestRoles                     5         1
 cal.RecurrentEvent                cal.RecurrentEvents                22        0
 cal.RemoteCalendar                cal.RemoteCalendars                7         0
 cal.Room                          cal.Rooms                          10        1
 cal.Subscription                  cal.Subscriptions                  4         0
 cal.Task                          cal.Tasks                          16        0
 calview.DailyPlannerRow           calview.DailyPlannerRows           7         2
 checkdata.Message                 checkdata.Messages                 6         0
 comments.Comment                  comments.Comments                  12        192
 comments.CommentType              comments.CommentTypes              4         0
 comments.Reaction                 comments.Reactions                 6         0
 contacts.Company                  contacts.Companies                 26        14
 contacts.CompanyType              contacts.CompanyTypes              7         10
 contacts.MembershipRequest        contacts.AllowMembershipRequests   5         0
 contacts.Partner                  contacts.Partners                  24        93
 contacts.Person                   contacts.Persons                   34        79
 contacts.Role                     contacts.Roles                     5         8
 contacts.RoleType                 contacts.RoleTypes                 5         11
 contacts.Worker                   contacts.BaseWorkers               35        9
 contenttypes.ContentType          gfks.ContentTypes                  3         88
 countries.Country                 countries.Countries                6         9
 countries.Place                   countries.Places                   9         80
 courses.AdministrationRoute       courses.AdministrationRouteTable   12        112
 courses.Course                    courses.Activities                 13        0
 courses.Enrolment                 courses.Enrolments                 18        0
 courses.Line                      courses.Lines                      42        1
 courses.MedicinePerPrescription   courses.MedicinesPerPrescription   5         0
 courses.Prescription              courses.Prescriptions              8         0
 courses.Slot                      courses.Slots                      7         0
 courses.Symptom                   courses.SymptomTable               7         0
 courses.SymptomPerPrescription    courses.SymptomsPerPrescription    3         0
 courses.Topic                     courses.Topics                     4         0
 excerpts.Excerpt                  excerpts.Excerpts                  11        0
 excerpts.ExcerptType              excerpts.ExcerptTypes              17        6
 groups.Group                      groups.Groups                      6         6
 groups.Membership                 groups.Memberships                 4         12
 invoicing.FollowUpRule            invoicing.FollowUpRules            4         0
 invoicing.Item                    invoicing.Items                    9         0
 invoicing.Plan                    invoicing.Plans                    8         0
 invoicing.SalesRule               invoicing.SalesRules               3         0
 invoicing.Tariff                  invoicing.Tariffs                  7         0
 ledger.Account                    ledger.Accounts                    18        20
 ledger.AccountingPeriod           ledger.AccountingPeriods           7         0
 ledger.FiscalYear                 ledger.FiscalYears                 5         9
 ledger.Journal                    ledger.Journals                    27        0
 ledger.LedgerInfo                 ledger.LedgerInfoTable             2         0
 ledger.MatchRule                  ledger.MatchRules                  3         0
 ledger.Movement                   ledger.Movements                   11        0
 ledger.PaymentTerm                ledger.PaymentTerms                11        8
 ledger.Voucher                    ledger.AllVouchers                 8         0
 linod.SystemTask              linod.SystemTasks              21        0
 measurements.Area                 measurements.AreaTable             3         0
 measurements.Distance             measurements.DistanceTable         3         0
 measurements.Time                 measurements.TimeTable             3         0
 measurements.Volume               measurements.VolumeTable           3         0
 measurements.Weight               measurements.WeightTable           3         0
 medico.PhysicianSpecialty         medico.PhysiciansSpecialties       3         0
 medico.Specialty                  medico.Specialties                 4         29
 memo.Mention                      memo.Mentions                      5         0
 notify.Message                    notify.Messages                    11        337
 orders.Enrolment                  orders.Enrolments                  5         0
 orders.Order                      orders.Orders                      34        0
 orders.OrderItem                  orders.OrderItems                  7         0
 products.Category                 products.Categories                15        62
 products.Medicine                 products.Medicines                 28        0
 products.PriceRule                products.PriceRules                4         0
 products.Product                  products.Products                  22        2
 products.Test                     products.Tests                     23        0
 sales.InvoiceItem                 sales.InvoiceItems                 15        0
 sales.PaperType                   sales.PaperTypes                   5         2
 sales.VatProductInvoice           sales.Invoices                     27        0
 sessions.Session                  users.Sessions                     3         ...
 system.SiteConfig                 system.SiteConfigs                 12        1
 tinymce.TextFieldTemplate         None                               5         2
 uploads.Upload                    uploads.Uploads                    13        0
 uploads.UploadType                uploads.UploadTypes                8         1
 uploads.Volume                    uploads.Volumes                    5         0
 users.Authority                   users.Authorities                  3         0
 users.User                        users.AllUsers                     25        15
 vat.InvoiceItem                   vat.InvoiceItemTable               9         0
 vat.VatAccountInvoice             vat.Invoices                       20        0
================================= ================================== ========= =======
<BLANKLINE>
