.. doctest docs/specs/general.rst
.. _medico.specs.general:

===============================
General overview of Lino Medico
===============================

The goal of Lino Medico is

.. contents::
  :local:


.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_medico.projects.medico1.settings.doctests')
>>> from lino.api.doctest import *


Show the list of members:

>>> rt.show(rt.models.users.AllUsers)
... #doctest: +NORMALIZE_WHITESPACE -REPORT_UDIFF
============ ==================
 First name   e-mail address
------------ ------------------
 Agnes
 Andy
 Bert
 Carlos
 Chloe
 Faruk        demo@example.com
 Michael
 Nazir        demo@example.com
 Olafur
 Parvez       demo@example.com
 Dan
 Robin        demo@example.com
 Rolf         demo@example.com
 Romain       demo@example.com
 Salma        demo@example.com
============ ==================
<BLANKLINE>

The `Products` table shows all products in alphabetical order:

>>> ses = rt.login("robin")
>>> ses.show(rt.models.products.Products)
... #doctest: +NORMALIZE_WHITESPACE -REPORT_UDIFF
No data to display
