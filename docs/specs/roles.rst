.. doctest docs/specs/roles.rst
.. _medico.specs.roles:

=========================
User roles in Lino Medico
=========================

>>> import lino
>>> lino.startup('lino_medico.projects.medico1.settings.doctests')
>>> from lino.api.doctest import *

Menus
-----

Site manager
------------------

Rolf is a :term:`site manager`, he has a complete menu.

>>> show_menu('robin')
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- Contacts : Patients, Physicians, Workers, Organizations, Membership Requests
- Calendar : My availability, Overdue appointments, My unconfirmed appointments, My guests, My overdue appointments, Calendar, Workers planner
- Reception : Appointments today, Waiting visitors, Busy visitors, Gone visitors, Visitors waiting for me
- Office : My Notification messages, My Excerpts, My Upload files
- Configure :
  - Lino daemon : System tasks
  - System : Users, Medical specialties, Groups, Site Parameters
  - Contacts : Legal forms, Functions
  - Calendar : Calendars, Rooms, Recurring events, Guest roles, Calendar entry types, Recurrency policies, Remote Calendars, Planner rows
  - Products : Products, Appointment, Transportation, Surgery, Bed, Medicine, Med utility kit, Product Categories, Price rules
  - Sales : Flatrates, Follow-up rules, Paper types
  - Office : Excerpt Types, Library volumes, Upload types
  - Places : Countries, Places
  - Accounting : Accounts, Journals, Fiscal years, Accounting periods, Payment terms
- Explorer :
  - Lino daemon : Task history
  - System : Authorities, User types, User roles, Group memberships, Notification messages, content types, Data checkers, Data problem messages
  - Contacts : Contact persons, Partners
  - Calendar : Calendar entries, Tasks, Presences, Subscriptions, Entry states, Presence states, Task states, Planner columns, Display colors
  - Orders : Orders, Enrolments
  - Products : Price factors
  - Sales : Invoicing plans, Sales rules, Invoicing areas, Sales invoices, Sales invoice items
  - Office : Mentions, Excerpts, Upload files, Upload areas
  - Accounting : Common accounts, Match rules, Vouchers, Voucher types, Movements, Trade types, Journal groups
  - VAT : VAT areas, VAT regimes, VAT classes, VAT columns, Invoices, VAT rules
- Site : About, User sessions
