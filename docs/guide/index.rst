.. _medico.guide:

======================
Lino Medico User Guide
======================

Welcome to Lino Medico user guide. We are thankful to you for deciding to tag
along with our journey to a better and brighter future.

This document contains usage and configuration details of healthcare facility in
lino-medico's centralized database system. Every healthcare facility manager
(designated person(s)) who shell (wish to) use lino-medico to maintain their
internal and external services should read this document thoroughly. Further
more, any other potential users should (may) also read this document for
convenient (ease of) usage of the lino-medico application.


.. toctree::
   :maxdepth: 1

Table of contents:

.. contents::
  :depth: 1
  :local:


Setting up context:

>>> from lino import startup
>>> startup('lino_medico.projects.medico1.settings.demo')
>>> from lino.api.doctest import *


User Types
==========

.. class:: UserTypes

  A subclass of :class:`ChoiceList <lino.core.choicelists.ChoiceList>` that
  contains each of the lino-medico :class:`UserType` instances given below.

.. class:: UserType

  A subclass of :class:`Choice <lino.core.choicelists.Choice>`. Instances of
  this class contains the defined :term:`role` -s / :term:`privilege` -s of a
  :class:`User`.

There are the following user types in the lino-medico application (concerning
the internal & external management of the activities within an indexed
healthcare facility): Patient, ReceptionClerk, Physician, OfficeStaff &
SiteStaff

There is also the user types Anonymous & SiteAdmin which has no ties to an
independently running healthcare facility.

The following are the choices, subclasses of :class:`UserType`, for user types
contained in :class:`UserTypes`:

.. class:: Anonymous

  Un- :term:`register` -ed users fall into this user type.

.. class:: Patient

  Any :term:`register` -ed user, by default, fall into the this user type.

.. class:: ReceptionClerk

  A :term:`register` -ed user, when raised the :term:`role` / :term:`privilege`,
  who can :term:`manage` / :term:`maintain` appointment queues, by a
  :class:`SiteStaff` or a :class:`SiteAdmin`.

.. class:: Physician

  A :term:`register` -ed user, when raised the :term:`role` / :term:`privilege`,
  who can :term:`manage` / :term:`maintain` activities relating to prescribing
  medication and/or performing surgeries and/or suggesting life style changes,
  such as, writing prescription, creating appointment schedules etc., by a
  :class:`SiteStaff` or a :class:`SiteAdmin`.

.. class:: OfficeStaff

  A :term:`register` -ed user, when raised the :term:`role` / :term:`privilege`,
  who can :term:`manage` / :term:`maintain` activities related to the proper
  operation of the associated healthcare facility, by a :class:`SiteStaff` or a
  :class:`SiteAdmin`.

.. class:: SiteStaff

  A :term:`register` -ed user or otherwise a :term:`virtual user` who has
  complete :term:`privilege` -s to the database entries and activities within a
  given healthcare facility.

.. class:: SiteAdmin

  A :term:`register` -ed user or otherwise a :term:`virtual user` who has
  complete access and :term:`privilege` -s to the lino-medico application.


Getting On Board
================

- Indexing a healthcare facility into lino-medico database system:

  Only a :class:`SiteAdmin` has the :term:`privilege` to index a healthcare
  facility into the lino-medico application.

  The internal name for healthcare facility('s) table in lino-medico is
  *Organizations*. Lino Medico consists of several smaller app module, among
  which *contacts* app contains the sources and functionalities related to
  *Organizations*. The :term:`main menu` in the lino-medico interface contains a
  menu entry for contacts (which in turn also leads to Organizations)
  management.

  .. class:: OrganizationType

    (Internal name is *CompanyType* which is an attribute to :class:`Company` as
    :attr:`Company.type`)

    There can be a variety of organization types when it comes to healthcare
    facilities. A few examples are (just by name and a short description):

    - Ambulance Service

      A vehicle specially equipped for taking sick or injured people to and
      from the hospital, especially in emergencies.

    - Organ and/or Blood bank

      Such a bank allows donors to donate Organ or blood while also storing and
      sorting components that can be used most effectively by patients.

    - Clinic and medical office

      A facility for diagnosis and treatment of :term:`outdoor patient`.

      Many people go to a clinic for routine doctor’s appointments and
      checkups. These healthcare facilities can be a physician’s private
      practice, a group practice setting or a corporately owned clinic that
      may be connected to a larger healthcare system or hospital.

    - Hospital

      Hospitals are the ultimate “catch-all” healthcare facility. Their services
      can vary greatly depending on their size and location, but a hospital’s
      goal is to save lives. Hospitals typically have a wide range of units that
      can be loosely broken into intensive care and non-intensive care units.

      Intensive care units deal with emergencies and the most serious illnesses
      and injuries. Patients with imminently life-threatening problems go here.

      Non-intensive care units include things like childbirth, surgeries,
      rehabilitation, step-down units for patients who have just been treated in
      intensive care and many others. Typically, most hospital beds could be
      classified as non-intensive care.

    - Imaging and radiology center

      These facilities, much like their hospital counterparts, offer diagnostic
      imaging services to patients. Diagnostic imaging includes CT scans,
      ultrasounds, X-rays, MRIs and more. While hospitals and even clinics have
      imaging centers, outpatient facilities help keep costs lower and allow
      more convenient scheduling for patients.

      Hospital facilities will likely handle imaging for urgent cases, such as
      an MRI for a brain injury. But any imaging that can be scheduled in
      advance, such as ultrasounds to monitor a pregnancy, could take place at
      an imaging center.

    - Pathological center

      These facilities deal with causes and effects of diseases, especially the
      branch of medicine that deals with the laboratory examination of samples
      of body tissue and/or fluid for diagnostic or forensic purposes.

    - Mental health and addiction treatment center

      These facilities deass Companyl with the patients having mental health issues or
      addiction.

    - Nursing home

      Nursing homes offer a living situation for patients whose medical needs
      aren’t severe enough for hospitalization, but are too serious to manage at
      home.

    - Emergency care

      This type of facilities deal with the patient who are in need of an
      immediate emergency room.

    - Urgent care

      Urgent care (UR) facilities exist for on-demand healthcare needs that
      aren’t severe enough for the emergency room, but are too severe or
      concerning to wait for a scheduled appointment at a doctor’s office.

    Source: `RASMUSSEN UNIVERSITY <https://www.rasmussen.edu/degrees/health-sciences/blog/types-of-healthcare-facilities/>`__

    Standard healthcare facility types [1]_ indexed by lino-medico:

    >>> rt.show('contacts.CompanyType')
    ============================================== ================== ==================
     Designation                                    Designation (de)   Designation (fr)
    ---------------------------------------------- ------------------ ------------------
     Ambulance Service
     Organ and/or Blood bank
     Clinic and medical office
     Hospital
     Imaging and radiology center
     Pathological center
     Mental health and addiction treatment center
     Nursing home
     Emergency care
     Urgent care
    ============================================== ================== ==================
    <BLANKLINE>

    P.S: If you are not sure in which of the above types your institution falls
    into please contact a lino-medico operator.

  If you are someone, who work in the administrative department of a healthcare
  facility and you decided to use lino-medico as tool for improving your
  institution's service quality and ease of access, you can contact one of the
  lino-medico operator to index you institution/(healthcare facility).

  Instructions to a lino-medico operator (with :class:`SiteAdmin`
  :term:`privilege`):

  - Sign In using your credentials
  - Select :menuselection:`Contacts --> Organizations` in the :term:`main menu`
  - Click on the :guilabel:`⊕` (circled plus) icon on the :term:`toolbar`
  - Fill out the :term:`insert window`
  - Click on the :guilabel:`Create` button at the bottom-right corner of the :term:`insert window`

  Giving Authorities to a healthcare facility administrator:

  - Select :menuselection:`Configure --> System --> Users` in the :term:`main menu`
  - Click on the :guilabel:`⊕` (circled plus) icon on the :term:`toolbar`
  - Fill out the necessary information in the :term:`insert window` where choose the newly created *Organization* as *Partner* and *500 (500 (Staff))* as *User type*
  - Click on the :guilabel:`Create` button at the bottom-right corner of the :term:`insert window`
  - Click on the :guilabel:`🖂` (envelop) icon on the :term:`toolbar` to let the user know about their lino-medico user account and credentials


Role & Role Types
=================

.. class:: Role

  A :class:`Role` binds a :class:`Person` to an healthcare facility, declaring
  their role within the healthcare facility.

  .. attribute:: person

    A pointer to a :class:`Person`.

  .. attribute:: company

    A pointer to the healthcare facility.

  .. attribute:: type

    A pointer to a :class:`RoleType`.

  .. attribute:: association_type

    The type of activity of the :class:`User` within the healthcare facility.
    Takes one of the following values:

    - indoor: A :class:`User` who only oversees the activities related to :term:`inpatient`
    - outdoor: A :class:`User` who only oversees the activities related to :term:`outpatient`
    - combined: A :class:`User` who oversees the activities related to both the :term:`inpatient` and the :term:`outpatient`

.. class:: RoleType

  Designation (role in the company) of an employee within a healthcare facility.
  Used internally to differentiate between types of employees.

  .. attribute:: name

    Designation of an employee.

  .. attribute:: can_sign

    A boolean type indicating whether an employee having this designation can
    dispatch any confidential document by signing it.

The following are some examples of role types within a healthcare facility:

- President: President of a healthcare facility.
- CEO: Chief executive officer of a healthcare facility.
- Director: Director of a healthcare facility.
- Secretary: Secretary of a healthcare facility.
- Manager: Manager of a healthcare facility.
- IT manager: IT manager of a healthcare facility.
- Physician: Physician of a healthcare facility.
- Reception Clark: Reception Clark of a healthcare facility.
- Office Staff: Office Staff of a healthcare facility.
- Supervisor: Supervisor of a healthcare facility.

.. OOTSU - Out Of The System User

.. _OOTSU:

- Nurse: Nurse of a healthcare facility.
- Cleaner: Cleaner of a healthcare facility.

There can be more :class:`RoleType` -s depending on the internal classification
criteria of a healthcare facility. To add such a :class:`RoleType` into the
lino-medico system, follow the instructions given below (Only a
:class:`SiteStaff` or a :class:`SiteAdmin` can add a :class:`RoleType`):

- Select :menuselection:`Configure --> Contacts --> Functions` in the :term:`main menu`
- Click on the :guilabel:`⊕`(circled plus) icon on the :term:`toolbar`
- Fill out the :term:`insert window`
- Click on the :guilabel:`Create` button at the bottom-right corner of the :term:`insert window`



Creating User & Person
======================

.. class:: User

  A lino-medico user, NOT necessarily a real person (can be a fictive identity),
  identified by unique :attr:`username` or :attr:`email`, whos :term:`role` -s
  and :term:`privilege` -s are defined as :attr:`user_type`.

  .. attribute:: email

    Email address of the :class:`User`.

  .. attribute:: language

    Language of the :class:`User`.

  .. attribute:: modified

    A datetime value specifying when the :class:`User` object was last modified.

  .. attribute:: created

    A datetime value specifying when the :class:`User` object was created.

  .. attribute:: start_date

    A datetime value specifying when the employment of the :class:`User` started.

  .. attribute:: end_date

    A datetime value specifying when the employment of the :class:`User` started.

  .. attribute:: username

    A unique string of characters by which lino-medico identifies the
    :class:`User`.

  .. attribute:: password

    Hash value of a secret string of characters by which lino-medico authorizes
    a :class:`User` while logging-in into the lino-medico system.

  .. attribute:: last_login

    A datetime value specifying when the :class:`User` last logged in.

  .. attribute:: user_type

    An instance of :class:`UserType` which contains the :term:`role` -s /
    :term:`privilege` -s of the :class:`User`.

  .. attribute:: initials

    The initials of the :class:`User` 's name.

  .. attribute:: first_name

    First name (or given name) of the :class:`User`.

  .. attribute:: last_name

    Last name (or family name) of the :class:`User`.

  .. attribute:: remarks

    Remarks (if any) for the :class:`User`.

  .. attribute:: partner

    A :term:`legal person` who maintains the financial activities of the
    :class:`User`. Either a :class:`Partner` or one of its subclasses,
    :class:`Person` or a *Company*.

  .. attribute:: verification_code

    A random string of characters generated by lino-medico when the
    :class:`User` was created and sent to the user's :attr:`email` for
    verification purposes.

  .. attribute:: verification_code_sent_on

    A datetime value at which the :attr:`verification_code` was sent to the
    :class:`User`.

  .. attribute:: time_zone

    The timezone at which the :class:`User` is located.

  .. attribute:: date_format

    The standard format for datetime within the :class:`User` 's locality.

  .. attribute:: dashboard_layout

    The :class:`User` 's chosen dashboard layout.

  .. attribute:: notify_myself

    A boolean value specifying whether to notify the :class:`User` of his own
    activity.

  .. attribute:: mail_mode

    A choice of intervals, specifying the frequency of notification to the
    :class:`User`.

  .. attribute:: person

    A real :class:`person <Person>` to whom the :class:`User` object belongs to.

  .. attribute:: company

    The company from where the :class:`User` object came to be.

  .. attribute:: access_class

    The class/type of calendar events the :class:`User` has access to.

  .. attribute:: event_type

    The default type for the :class:`User` on calendar entries.


.. class:: Partner

  An object of this class represents a :term:`legal person` either a company or
  a real :class:`Person`.

  .. attribute:: email

    The contact email for the :class:`Partner`.

  .. attribute:: language

    Probably NOT RELATED

  .. attribute:: url

    Link to external pages of the :class:`Partner`.

  .. attribute:: phone

    Phone number of the :class:`Partner`

  .. attribute:: gsm

    Mobile phone number of the :class:`Partner`.

  .. attribute:: fax

    The fax number of the :class:`Partner`.

  .. attribute:: country

    The country of the :class:`Partner` 's :term:`HQ`.

  .. attribute:: city

    The city of the :class:`Partner` 's :term:`HQ`.

  .. attribute:: zip_code

    The postal code of the :class:`Partner` 's :term:`HQ`.

  .. attribute:: region

    The state or division of the :class:`Partner` 's :term:`HQ`, which wikidata
    specifies as `first-level administrative country subdivision <https://www.wikidata.org/wiki/Q10864048>`__.

  .. attribute:: addr1

    The address-line-1 of the :class:`Partner` 's :term:`HQ`.

  .. attribute:: street_prefix

    The prefix (if any) for the street line of the :class:`Partner` 's :term:`HQ`.

  .. attribute:: street

    The street name of the :class:`Partner` 's :term:`HQ`.

  .. attribute:: street_no

    The house number of the :class:`Partner` 's :term:`HQ`.

  .. attribute:: street_box

    The box number for the postal mail (snail mail) of the :class:`Partner` 's
    :term:`HQ`.

  .. attribute:: addr2

    The address-line-2 of the :class:`Partner` 's :term:`HQ`.

  .. attribute:: prefix

    The name prefix of the :class:`Partner`.

  .. attribute:: name

    The name of the :class:`Partner`.

  .. attribute:: remarks

    Remarks to the :class:`Partner` (if any).


.. class:: Company

  Represents a healthcare facility and to do so this model inherits all the
  attributes from :class:`Partner`.

  Additional attributes:

  .. attribute:: type

    Takes one of the values from :class:`OrganizationType`


.. class:: Person

  Unlike a :class:`User`, a :class:`Person` object represents a real person with
  a national identification number and does NOT have any attributes relating to
  :term:`role` / :term:`privilege`. A :class:`Person` inherits all of the fields
  from :class:`Partner`.

  .. attribute:: partner_ptr

    A pointer to the parent :class:`Partner` object.

  .. attribute:: title

    Probably NOT RELATED

  .. attribute:: first_name

    See: :attr:`User.first_name`.

  .. attribute:: middle_name

    The middle name of the :class:`Person`.

  .. attribute:: last_name

    See: :attr:`User.last_name`

  .. attribute:: gender

    The gender of the :class:`Person`.

  .. attribute:: birth_date

    Date of birth of the :class:`Person`.

  .. attribute:: national_id

    National Identification Number of the :class:`Person`.

  .. attribute:: nationality

    Nationality of the :class:`Person`.


.. class:: Worker

  A person who is an employee of a healthcare facility. Inherits all of the
  attributes of :class:`Person`.


Note: The subtle differences between a :class:`User`, a :class:`Person` and a
:class:`Partner` is that a :class:`User` can be (but NOT necessarily) a fictive
identity but a :class:`Person` must represent a real person whereas a
:class:`Partner` is a :term:`legal person` (immaterial to whether a real person
or a company).

The objects :class:`User` & :class:`Person` are exclusively independent of each
other, so it is possible to have different combinations of them while
representing a certain entity.

- The possible combinations of :class:`User` & :class:`Person`:

.. CASE_1:

CASE 1: :class:`User` only

  A :term:`virtual user`.
  In probably a very few cases we might have a :class:`User` object that does
  NOT have associated :class:`Person` and/or :class:`Partner` object(s). For
  example, we might have to give access to some other program/application and in
  such a case, we might create a :class:`User` object and by using the
  credentials from this :class:`User`, the scrutinized program/application may
  communicate with lino-medico system.

.. CASE_2:

CASE 2: :class:`Person` only

  In some cases, a healthcare facility might have some employee such as a
  :ref:`Cleaner <OOTSU>` who does NOT need access to lino-medico system and the
  healthcare facility needs to keep a record of such employee's activities where
  the healthcare facility itself is the :term:`legal person` for the employee,
  such a case is when the healthcare facility may create a :class:`Person`
  object into the lino-medico system in order to maintain records.

.. CASE_3:

CASE 3: :class:`User` and :class:`Person`

  When a user signs up from the GUI (frontend), lino-medico creates an
  :class:`User` object as well as a :class:`Person` object. While creating the
  :class:`User`, lino-medico sets the :attr:`User.user_type` attribute to
  :class:`Patient`.

- Signing-up in lino-medico:

  Make sure you are NOT already logged-in into the lino-medico system

  - Select :menuselection:`Anonymous⚙ --> ⊕Create Account` in the :term:`main menu`
  - Fill out the :term:`insert window`
  - Click on the :guilabel:`Ok` button at the bottom-right corner of the :term:`insert window`

After signing-up, lino-medico will automatically sign you in into the system. At
the moment the :class:`User` has the :term:`role` / :term:`privilege` of a
:class:`Patient`. If the user wish to (shell) become a member (either a
:class:`Physician` or with any other :class:`Role`) of the healthcare facility,
they can request for a membership.

.. class:: MembershipRequest

  .. attribute:: user

    The :class:`User` who made the :class:`MembershipRequest`

  .. attribute:: association

    The :class:`Company` to which the :class:`User` is requesting for a
    membership

  .. attribute:: role_type

    See :attr:`Role.type`

  .. attribute:: association_type

    See :attr:`Role.association_type`.


- Request for a membership:

  - Select :menuselection:`Your name⚙ --> Profile & Preferences` in the :term:`main menu` [2]_
  - Click on the :guilabel:`Request membership` button on the :term:`toolbar`
  - In the :term:`insert window` select your desired :guilabel:`Healthcare facility` and your :guilabel:`Function` in the healthcare facility
  - Click on the :guilabel:`Create` button at the bottom-right corner of the :term:`insert window`

This will submit a membership request available to the healthcare facility
administration for review.

- Review membership request:

  :class:`OfficeStaff` or other :class:`User` with higher :term:`privilege` can
  review (:guilabel:`Approve` or :guilabel:`Deny`) a membership request.

  - Select :menuselection:`Contacts --> Membership Requests` in the :term:`main menu`
  - Select the row(s) to approve or deny the request(s)
  - Click :guilabel:`Approve` or :guilabel:`Deny` on the :term:`toolbar` to approve or deny the request(s)

When a :class:`MembershipRequest` is approved, lino-medico will replace the
associated :class:`Person` of the :class:`User` instance with a :class:`Worker`
instance.


While a :term:`register` -ed (by signing up from the frontend, which also
creates a :class:`Person` instance) :class:`User` can make a membership request,
a authorized (by the healthcare facility) :class:`User` can create
:class:`Worker` (s), :class:`User` (s) and :class:`Role` (s) from the
lino-medico frontend (GUI).


- Creating a :class:`Worker`:

  :class:`OfficeStaff` or :class:`User` (s) with higher :term:`privilege` can
  create a :class:`Worker`

  - Select :menuselection:`Contacts --> Workers` in the :term:`main menu`
  - Click on the :guilabel:`⊕` (circled plus) icon on the :term:`toolbar`
  - Fill out the :term:`insert window`
  - Click on the :guilabel:`Create` button at the bottom-right corner of the :term:`insert window`

- Creating a :class:`User`:

  Only a :class:`SiteStaff` can create :class:`User` as their healthcare
  facility specific employee

  - Select :menuselection:`Configure --> System --> Our Users` in the :term:`main menu`
  - Click on the :guilabel:`⊕` (circled plus) icon on the :term:`toolbar`
  - Fill out the :term:`insert window`
  - Click on the :guilabel:`Create` button at the bottom-right corner of the :term:`insert window`

- Adding medical specialty to :class:`Worker` (i.e. physician):

  This can be done in two ways, a :class:`SiteStaff` or a :class:`SiteAdmin` can
  add a set of specialties to a physician or a :class:`Physician` him/her-self
  can add specialties to their corresponding :class:`Worker` object.

  Standard specialties that are available on lino-medico:

  >>> rt.show('medico.Specialty')
  ==== ====================================== ================== ==================
   ID   Designation                            Designation (de)   Designation (fr)
  ---- -------------------------------------- ------------------ ------------------
   1    Allergy and Immunologist
   2    Anesthesiologist
   3    Cardiologist
   4    Dermatologist
   5    Endocrinologist
   6    Medicine
   7    Emergency medicine
   8    Family medicine
   9    Internal medicine
   10   Preventive medicine
   11   Physical medicine and rehabilitation
   12   Medical genetics
   13   Neurologist
   14   Nuclear medicine
   15   Obstetrics and gynecologist
   16   Ophthalmologist
   17   Surgeon
   18   Orthopedic surgeon
   19   Pathologist
   20   Pediatrics
   21   Plastic surgeon
   22   Psychiatrist
   23   Radiation oncologist
   24   Radiologist
   25   Sleep Medicine
   26   Urologist
   27   Neurosurgeon
   28   Otolaryngologist
   29   Thoracic surgeon
  ==== ====================================== ================== ==================
  <BLANKLINE>

  :class:`SiteStaff` (s) and :class:`SiteAdmin` (s) can add more specialties [1]_ as
  the desire.


  Instructions for :class:`SiteStaff` and :class:`SiteAdmin`:

  - Select :menuselection:`Contacts --> Physicians` in the :term:`main menu`
  - Double click on the of the desired physician (on the Physicians table) to open the row in a :term:`detail window`
  - In the **Physician's specialties** table click on the :term:`phantom row` 's **Medical specialty** column
  - Select one of the specialties from the suggestions
  - Press RETURN button on the keyboard

  Instructions for a :class:`Physician`:

  - Select :menuselection:`Your name⚙ --> Profile & Preferences` in the :term:`main menu` [2]_
  - Click on the :guilabel:`Employment` tab on the tabbed :term:`detail window`
  - In the **Physician's specialties** table click on the :term:`phantom row` 's **Medical specialty** column
  - Select one of the specialties from the suggestions
  - Press RETURN button on the keyboard


Services and Products
=====================

When a physician, appointed in a healthcare facility, looks after some
:term:`outpatient`, the physician can create a schedule for when and where
(within the premises of the healthcare facility) will he/she be available so
that a :class:`patient <Patient>` can fix an appointment ahead of time.

Creating an appointment schedule (as a :class:`physician <Physician>`):

- Select :menuselection:`Activities --> My schedule` in the :term:`main menu`
- Click on the :guilabel:`⊕` (circled plus) icon on the :term:`toolbar`
- Fill out the :term:`insert window`
- Click on the :guilabel:`Create` button at the bottom-right corner of the :term:`insert window`

Creating an appointment schedule (as a :class:`site staff <SiteStaff>`):

- Select :menuselection:`Activities --> Physician's schedule` in the :term:`main menu`
- Click on the :guilabel:`⊕` (circled plus) icon on the :term:`toolbar`
- Fill out the :term:`insert window`
- Click on the :guilabel:`Create` button at the bottom-right corner of the :term:`insert window`

Creating such an appointment schedule will also automatically generate some
calendar entries which the :class:`physician <Physician>` can checkout by:

- Selecting :menuselection:`Calendar --> My appointments` in the :term:`main menu`

These calendar entries (appointments) will NOT be visible to the public by
default. To make such a calendar entry visible to the public, the physician will
have to change the workflow status of the entry from :guilabel:`Suggested` to
:guilabel:`Published` by clicking on the :guilabel:`☼` button from the
:guilabel:`Workflow` column.

In the case, when the :class:`physician <Physician>` will NOT be present (for
whatever the reason may be, i.e. the physician him/her-self could get sick or
have some other business to deal with) for such an appointment (calendar entry),
the physician can click on the :guilabel:`☒` button (from the
:guilabel:`Workflow` column) to cancel the appointment.

These sort of appointment schedules can have a span of more then a few hours,
which can be specified at the time of creating the appointment schedule or can
be modified from the appointment schedule's :term:`detail window`. Whereas to
take care of a :class:`patient <Patient>` the physician might only take several
minutes. And so in goes that, during such an appointment schedule the physician
will be able to take care of many :class:`patient <Patient>` -s. It can be
specified in an appointment schedule for a patient how much time on average the
physician would spend by filling out the value for :guilabel:`average dispatch
time`, so that the time for the arrival of the visitors/patients could be
arranged appropriately.



Brainstorming with Luc: https://luc.lino-framework.org/blog/2022/0317.html


A physician can say to a patient: "Let's make a liver test on you", which means
that the doctor would create a :term:`pathological test` for this patient, with
one item, pointing to the :term:`pathology` labeled "Liver test".

This means in reality that the patient will have one medical care procedure
where the nurse takes one syringe of blood and fills it into three bottles
(bilirubin, LL, fat). And two of these bottles go to one laboratory, the third
one goes to another laboratory. The physician must then wait until the three
results are available. It might happen that one of the bottles gets lost. After
some time the physician will want to call the laboratory. And maybe he will have
to say to the patient "Sorry, you must get another blood test". This time it
won't be three bottles, only the one that got lost.


Draft
=====

- reception : add a feature to define
  - multiple waiting rooms. Maybe use cal.Room? Attention: Lino Welfare uses reception, and end users are very conservative

  OBSERVATION: The reception plugin only modifies the cal.Guest model (adds a
  few fields and a few actions). Each guest instance is linked to a cal.Event
  instance and each event instance can have a cal.Room instance. If we consider
  a room instance as a reception we can filter the events and as well as the
  guests.

NB: dispatcher "waiting ticket dispensing machine" is outside of Lino

Add at  least ledger plugin for the TradeTypes (laboratoy = purchases, patients = sales, doctors = wages...)

One ledger.Journal "Laboratory tests". with voucher type "Test order". We will have a new plugin "diagnostics". Which is similar to orders plugin.

A diag order contains:

course
date requested:
due_date
state
date_received_result
uploads_by_owner

a list of items, each item with
- type of analysis (pointer to product)

On every patient there is an action "Order diags test"

Where to store a pointer to the



.. glossary::

  ICD

    `International Classification of Diseases
    <https://www.who.int/news-room/spotlight/international-classification-of-diseases>`__
    (`API-doc <https://icd.who.int/icdapi/>`__ for developers.)

  register

    Any real person with country wise identification (e.g. SSIN, NID etc.)
    number creating an account in the lino-medico application.

  virtual user

    A fictive identity on the lino-medico application of a real person or a
    program.

  role

    A designation to identify a :term:`register` -ed user or otherwise
    :term:`virtual user` as his/her type (:term:`user type`).

  privilege

    Access and capabilities of a :term:`register` -ed user or a :term:`virtual user`.

  manage

    The :term:`privilege` to create, delete and/or update database entries
    and/or activities.

  maintain

    The :term:`privilege` to look after an activity for it's proper execution
    and in doing so may update the activity and/or database entry.

  outdoor patient

    A patient who receives medical treatment without being admitted to a
    hospital. Sometimes also referred to as :term:`outpatient`.

  outpatient

    Same as :term:`outdoor patient`.

  indoor patient

    A patient who stays in a hospital while under treatment. Sometimes also
    referred to as :term:`inpatient`.

  inpatient

    Same as :term:`indoor patient`.

  legal person

    A person or a company whom the local government recognizes as an entity
    subjectable to taxation.

  HQ

    Headquarters

  pathological test

    A process or test on a given patient in order to get information about one
    or several :term:`pathology`.

    Stored in :class:`lino_xl.lib.orders.Order`.

  pathology

    The predicted or actual progression of a particular disease.

    Stored in :class:`lino_medico.lib.products.Pathology`

A physician can say to a patient: "Let's make a liver test on you", which means
that the doctor would create a :term:`pathological test` for this patient, with
one item, pointing to the :term:`pathology` labelled "Liver test".

This means in reality that the patient will have one medical care procedure
where the nurse takes one syringe of blood and fills it into three bottles
(bilirubine, LL, fat). And two of these bottles go to one laboratory, the third
one goes to another laboratory. The physician must then wait until the three
results are available. It might happen that one of the bottles gets lost. After
some time the physician will want to call the laboratory. And maybe he will have
to say to the patient "Sorry, you must get another blood test". This time it
won't be three bottles, only the one that got lost.





.. rubric:: Footnotes

.. [1] The plan for the future is to have a regulatory authority authorized by the government who will look after the standardization of such a scrutinized set of things.
.. [2] Where, *Your name* literally means the name of the user.
