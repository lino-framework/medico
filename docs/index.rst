.. _medico:

===========
Lino Medico
===========

Welcome to the **Lino Medico** project homepage.


Content
========

.. toctree::
   :maxdepth: 1

   blog/index
   guide/index
   install/index
   specs/index
   api/index
   changes
