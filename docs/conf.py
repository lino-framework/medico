# -*- coding: utf-8 -*-
from atelier.sphinxconf import configure ; configure(globals())
from lino.sphinxcontrib import configure
configure(globals(), 'lino_medico.projects.medico1.settings.doctests')

extensions += ['lino.sphinxcontrib.base']  # for tcname
extensions += ['rstgen.sphinxconf.blog']
extensions += ['lino.sphinxcontrib.logo']

extensions += ['lino.sphinxcontrib.help_texts_extractor']
help_texts_builder_targets = {
    'lino_medico.': 'lino_medico.lib.medico'
}
show_authors = True
project = "Lino Medico"
copyright = '2016-2022 Rumma & Ko Ltd'

html_title = "Lino Medico"
# html_context.update(public_url='https://medico.lino-framework.org')
