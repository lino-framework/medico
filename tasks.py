from atelier.invlib import setup_from_tasks
ns = setup_from_tasks(
    globals(), "lino_medico",
    languages=("en", "bn", "de", "fr"),
    blog_root='docs',
    blogref_url='https://lino-framework.gitlab.io/medico',
    # tolerate_sphinx_warnings=True,
    locale_dir='lino_medico/lib/medico/locale',
    revision_control_system='git',
    cleanable_files=['docs/api/lino_medico.*'],
    demo_projects=['lino_medico.projects.medico1'])
