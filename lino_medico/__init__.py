# -*- coding: UTF-8 -*-
# Copyright 2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""This is the main module of Lino Medico.

.. autosummary::
   :toctree:

   lib
   projects


"""

from .setup_info import SETUP_INFO

__version__ = SETUP_INFO.get('version')

intersphinx_urls = dict(docs="https://lino-framework.gitlab.io/medico")
srcref_url = 'https://gitlab.com/lino-framework/medico/blob/master/%s'
doc_trees = ['docs']










