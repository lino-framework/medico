# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""
The main plugin for Lino Medico.

.. autosummary::
   :toctree:

    models
    migrate
    user_types

"""

from lino.api import ad, _

class Plugin(ad.Plugin):

    def setup_config_menu(self, site, user_type, m):
        mg = site.plugins.system
        m = m.add_menu(mg.app_label, mg.verbose_name)
        m.add_action('medico.Specialties')
