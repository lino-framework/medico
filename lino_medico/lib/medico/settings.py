# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

import json
from importlib import import_module
from os.path import join, dirname, exists
from lino.projects.std.settings import *
from lino_medico import SETUP_INFO

class Site(Site):

    verbose_name = "Lino Medico"
    description = SETUP_INFO['description']
    version = SETUP_INFO['version']
    url = SETUP_INFO['url']

    default_build_method = 'appypdf'

    demo_fixtures = ['std', 'demo', 'demo2']
    user_types_module = 'lino_medico.lib.medico.user_types'
    migration_class = 'lino_medico.lib.medico.migrate.Migrator'
    custom_layouts_module = 'lino_medico.lib.medico.layouts'

    # use_linod = True

    def get_database_settings(self):
        conf_path = join(dirname(__file__), 'db_conf.json')
        if exists(conf_path):
            with open(conf_path) as f:
                return json.load(f)
        return super().get_database_settings()

    def get_installed_plugins(self):
        """Implements :meth:`lino.core.site.Site.get_installed_plugins`.

        """
        yield super(Site, self).get_installed_plugins()
        yield 'lino_medico.lib.users'
        # yield 'lino_medico.lib.lets'
        yield 'lino_medico.lib.medico'
        yield 'lino_medico.lib.contacts'

        # TODO: configure households and humanlinks
        # yield 'lino_medico.lib.households'
        # yield 'lino_xl.lib.humanlinks'

        yield 'lino_xl.lib.groups'  # teams of workers
        yield 'lino_medico.lib.cal'
        yield 'lino_xl.lib.calview'
        yield 'lino_medico.lib.reception'
        yield 'lino_medico.lib.courses'
        yield 'lino_xl.lib.orders'
        yield 'lino_xl.lib.measurements'
        yield 'lino_medico.lib.products'
        yield 'lino_xl.lib.invoicing'
        # yield 'lino_xl.lib.tickets'
        yield 'lino.modlib.comments'
        yield 'lino.modlib.notify'
        yield 'lino.modlib.search'
        # utilities
        yield 'lino_xl.lib.excerpts'
        yield 'lino.modlib.export_excel'
        yield 'lino.modlib.tinymce'
        yield 'lino.modlib.weasyprint'
        yield 'lino_xl.lib.appypod'

    def get_plugin_configs(self):
        yield super().get_plugin_configs()
        yield ('system', 'use_dashboard_layouts', True)
        yield ('users', 'allow_online_registration', True)


    # def setup_quicklinks(self, user, tb):
    #     super(Site, self).setup_quicklinks(user, tb)
    #     tb.add_action('lets.Products')
