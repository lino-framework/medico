# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""Defines the standard user types for Lino Medico."""


from lino.core.roles import UserRole, SiteAdmin, SiteUser, SiteStaff
from lino.modlib.users.choicelists import UserTypes
from django.utils.translation import gettext_lazy as _

from lino.modlib.office.roles import OfficeStaff, OfficeUser, OfficeOperator
from lino.modlib.search.roles import SiteSearcher
from lino_xl.lib.cal.roles import GuestOperator
from lino_xl.lib.contacts.roles import ContactsUser, ContactsStaff
from lino_xl.lib.courses.roles import CoursesUser, CoursesTeacher
from lino_xl.lib.excerpts.roles import ExcerptsUser, ExcerptsStaff
from lino_xl.lib.accounting.roles import (LedgerPartner, AccountingReader,
    LedgerUser, VoucherSupervisor, LedgerStaff)
from lino_xl.lib.notes.roles import NotesUser, NotesStaff
from lino_xl.lib.products.roles import ProductsUser, ProductsStaff
from lino_xl.lib.topics.roles import TopicsUser

from lino_medico.lib.reception.roles import ReceptionUser, ReceptionOperator, ReceptionStaff


class Patient(SiteUser, ReceptionUser, SiteSearcher, LedgerPartner):
    pass

class ReceptionClerk(SiteUser, ReceptionOperator, SiteSearcher, OfficeOperator,
                    GuestOperator, AccountingReader):
    pass

class Physician(SiteUser, ContactsUser, OfficeUser, OfficeOperator,
                GuestOperator, TopicsUser, ReceptionUser, ReceptionOperator,
                CoursesUser, CoursesTeacher, ExcerptsUser, LedgerUser,
                ProductsUser, SiteSearcher):
    pass


class OfficeStaff(SiteUser, OfficeStaff, SiteSearcher, VoucherSupervisor):
    pass


class SiteStaff(SiteStaff, ContactsStaff, OfficeStaff, ReceptionStaff,
                ExcerptsStaff, ProductsStaff, LedgerStaff):
    pass


class SiteAdmin(SiteAdmin, ContactsStaff, OfficeStaff,
                GuestOperator, TopicsUser, ReceptionStaff,
                CoursesUser, CoursesTeacher, LedgerStaff,
                ExcerptsStaff, ProductsStaff):
    pass


UserTypes.clear()
add = UserTypes.add_item
add('000', _("Anonymous"),        UserRole, 'anonymous',
    readonly=True, authenticated=False)
add('100', _("User"),             Patient,  'user')
add('200', _("Reception clerk"),  ReceptionClerk, 'receptionclerk')
add('300', _("Physician"),        Physician, 'physician')
add('500', _("Office staff"),     OfficeStaff, 'officestaff')
add('600', _("Site staff"),       SiteStaff, 'staff')
add('900', _("Administrator"),    SiteAdmin, 'admin')

ROLE_TO_TYPE = dict()
ROLE_TO_TYPE['Secretary'] = UserTypes.staff
ROLE_TO_TYPE['CEO'] = UserTypes.staff
ROLE_TO_TYPE['Director'] = UserTypes.staff
ROLE_TO_TYPE['IT manager'] = UserTypes.staff
ROLE_TO_TYPE['Office staff'] = UserTypes.officestaff
ROLE_TO_TYPE['Supervisor'] = UserTypes.officestaff
ROLE_TO_TYPE['Reception clerk'] = UserTypes.receptionclerk
ROLE_TO_TYPE['Physician'] = UserTypes.physician

def get_user_type_from_role(role_name):
    if role_name not in ROLE_TO_TYPE:
        return None
    return ROLE_TO_TYPE[role_name]
