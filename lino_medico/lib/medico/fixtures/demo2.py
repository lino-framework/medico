# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import rt, _

from django.conf import settings

demo_users = settings.SITE.demo_users

def objects():
    users = rt.models.users.User.objects.filter(username__in=demo_users)
    LGH = rt.models.contacts.Company.objects.get(name='Laksham General Hospital')

    for user in users:
        user.association = LGH
        yield user

    users = rt.models.users.User.objects.exclude(association__isnull=False)
    default = rt.models.contacts.Company.objects.get(remarks='default')

    for user in users:
        user.association = default
        yield user
