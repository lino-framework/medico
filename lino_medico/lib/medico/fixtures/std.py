# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import rt, _

def objects():
    Specialty = rt.models.medico.Specialty

    yield Specialty(designation=_('Allergy and Immunologist'))
    yield Specialty(designation=_('Anesthesiologist'))
    yield Specialty(designation=_('Cardiologist'))
    yield Specialty(designation=_('Dermatologist'))
    yield Specialty(designation=_('Endocrinologist'))
    yield Specialty(designation=_('Medicine'))
    yield Specialty(designation=_('Emergency medicine'))
    yield Specialty(designation=_('Family medicine'))
    yield Specialty(designation=_('Internal medicine'))
    yield Specialty(designation=_('Preventive medicine'))
    yield Specialty(designation=_('Physical medicine and rehabilitation'))
    yield Specialty(designation=_('Medical genetics'))
    yield Specialty(designation=_('Neurologist'))
    yield Specialty(designation=_('Nuclear medicine'))
    yield Specialty(designation=_('Obstetrics and gynecologist'))
    yield Specialty(designation=_('Ophthalmologist'))
    yield Specialty(designation=_('Surgeon'))
    yield Specialty(designation=_('Orthopedic surgeon'))
    yield Specialty(designation=_('Pathologist'))
    yield Specialty(designation=_('Pediatrics'))
    yield Specialty(designation=_('Plastic surgeon'))
    yield Specialty(designation=_('Psychiatrist'))
    yield Specialty(designation=_('Radiation oncologist'))
    yield Specialty(designation=_('Radiologist'))
    yield Specialty(designation=_('Sleep Medicine'))
    yield Specialty(designation=_('Urologist'))
    yield Specialty(designation=_('Neurosurgeon'))
    yield Specialty(designation=_('Otolaryngologist'))
    yield Specialty(designation=_('Thoracic surgeon'))
