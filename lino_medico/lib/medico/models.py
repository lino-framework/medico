# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""The :xfile:`models.py` module for this plugin.

"""

from lino.api import rt, dd, _
from lino.mixins import BabelDesignated
from lino.core import constants

from lino_medico.lib.medico.user_types import SiteAdmin, SiteStaff, Physician, Patient, UserTypes


class Specialty(BabelDesignated):
    class Meta:
        app_label = 'medico'
        abstract = dd.is_abstract_model(__name__, 'Specialty')
        verbose_name = _("Medical specialty")
        verbose_name_plural = _("Medical specialties")

# rename
class PhysicianSpecialty(dd.Model):
    class Meta:
        app_label = 'medico'
        abstract = dd.is_abstract_model(__name__, "PhysicianSpecialty")
        verbose_name = _("Physician specialty")
        verbose_name_plural = _("Physician specialties")

    physician = dd.ForeignKey('contacts.Worker', related_name='specialties_by_physician')
    specialty = dd.ForeignKey('medico.Specialty')

    @dd.chooser()
    def specialty_choices(cls, physician, ar):
        if physician is None:
            physician = ar.master_instance

        return rt.models.medico.Specialty.objects.exclude(
            pk__in=cls.objects.filter(physician=physician).values_list('specialty'))


class Specialties(dd.Table):
    model = 'medico.Specialty'
    label = _("Medical specialties")
    columns = "designation *"
    insert_layout = """
    designation
    """
    required_roles = dd.login_required((SiteStaff, SiteAdmin))

class PhysiciansSpecialties(dd.Table):
    model = 'medico.PhysicianSpecialty'
    label = _("Physician's specialties")
    insert_layout = """
    physician specialty
    """
    required_roles = dd.login_required((SiteStaff, SiteAdmin))

class SpecialtiesByPhysician(PhysiciansSpecialties):
    master_key = 'physician'
    required_roles = dd.login_required((SiteStaff, SiteAdmin, Physician))

class SpecialtiesByPhysicianToEvent(SpecialtiesByPhysician):
    allow_create = False
    required_roles = dd.login_required((Patient,))
    column_names = 'specialty__designation'
    display_mode = ((None, constants.DISPLAY_MODE_STORY), )

    @classmethod
    def get_master_instance(cls, ar, model, pk):
        if not pk:
            return None
        return rt.models.cal.Event.objects.get(pk=pk).user.worker

class SpecialtiesByUser(SpecialtiesByPhysician):

    @classmethod
    def get_master_instance(cls, ar, model, pk):
        user = ar.get_user()
        assert user.user_type == UserTypes.physician
        return user.worker


class PhysiciansBySpecialty(PhysiciansSpecialties):
    master_key = 'specialty'
    allow_create = False
    required_roles = dd.login_required()
