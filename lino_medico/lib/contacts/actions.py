# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

import random
from django.db import models

from lino.api import rt, dd, _
from lino.utils import mti
from lino.mixins.periods import Weekly
from lino.modlib.printing.actions import DirectPrintAction

from lino_medico.lib.medico.user_types import OfficeStaff, get_user_type_from_role


class PrintRoster(DirectPrintAction):
    help_text = _("Print a roster of calendar events")
    # combo_group = "creacert"
    label = _("Roster")
    tplname = "roster"
    build_method = "weasy2pdf"
    icon_name = None
    show_in_toolbar = False
    parameters = Weekly(
        show_remarks=models.BooleanField(
            _("Show remarks"), default=False),
        overview=models.BooleanField(
            _("Overview"), default=True))
    params_layout = """
    start_date
    end_date
    overview
    show_remarks
    """
    # keep_user_values = True

class ApproveMembership(dd.Action):
    select_rows = True
    label = _("Approve")
    required_roles = dd.login_required(OfficeStaff)

    def run_from_ui(self, ar, **kwargs):
        org = ar.get_user().association
        rows = ar.selected_rows
        Role = rt.models.contacts.Role
        Person = rt.models.contacts.Person
        Worker = rt.models.contacts.Worker
        Authority = rt.models.users.Authority
        SYNCHRONIZED_FIELDS = rt.models.users.SYNCHRONIZED_FIELDS
        for row in rows:
            user_type = get_user_type_from_role(row.role_type.name)
            user = row.user

            if user_type is not None:
                username = user.username
                user.association = org
                user.id = None
                user.username = username + '_' + str(random.randint(1111, 9999))
                user.user_type = user_type

                old_user = rt.models.users.User.objects.get(username=username)

                if old_user.partner is None:
                    w_attr = dict()
                    for fld in SYNCHRONIZED_FIELDS:
                        w_attr[fld] = getattr(user, fld)
                    worker = Worker(**w_attr)
                    worker.full_clean()
                    worker.save()
                    user.partner = worker
                elif old_user.person is None:
                    person = mti.insert_child(
                        old_user.partner, Person, full_clean=True)
                    worker = mti.insert_child(person, Worker, full_clean=True)
                elif old_user.worker is None:
                    worker = mti.insert_child(
                        old_user.person, Worker, full_clean=True)
                else:
                    worker = user.worker

                user.full_clean()
                user.save()
                # TODO: Send welcome email

                if old_user.association is None or old_user.association == user.association:
                    authority = Authority(authorized=user,
                        user=old_user)
                    authority.full_clean()
                    authority.save()
                    for auth in Authority.objects.filter(authorized=old_user):
                        authority = Authority(authorized=user, user=auth.user)
                        authority.full_clean()
                        authority.save()
            else:
                w_attr = dict()
                for fld in SYNCHRONIZED_FIELDS:
                    w_attr[fld] = getattr(user, fld)
                worker = Worker(**w_attr)
                worker.full_clean()
                worker.save()

            role, _ = Role.objects.get_or_create(company=org, person=worker)
            role.type=row.role_type
            role.association_type=row.association_type
            role.full_clean()
            role.save()
            row.delete()
        ar.success(refresh=True)


class DenyMembership(ApproveMembership):
    label = _("Deny")

    def run_from_ui(self, ar, **kwargs):
        for row in ar.selected_rows:
            row.delete()
        ar.success(refresh=True)
