# -*- coding: UTF-8 -*-
# Copyright 2013-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

import copy
import datetime
from django.db.models import (Q, ManyToManyField, OuterRef, Exists, Case, Value,
                            When, BooleanField, ForeignKey, Field, CASCADE)
from django.conf import settings
from lino.utils.html import E

from lino.api import dd, rt, _
from lino.utils.quantities import ZERO_DURATION
from lino.utils import SumCollector

from lino_xl.lib.contacts.models import *
from lino_xl.lib.contacts.roles import ContactsUser

#****** Country specific import ******#
# from lino_xl.lib.beid.mixins import SSIN
from lino_xl.lib.bnid.mixins import SSIN

from lino_xl.lib.calview.mixins import Plannable
from lino.modlib.users.mixins import UserAuthored

from lino_xl.lib.calview.models import ParameterClone
from lino_xl.lib.calview.models import WeeklySlaveBase, DailySlaveBase
from lino_xl.lib.calview.models import Planners, CalendarView, InsertEvent

from .choicelists import AssociationTypes, association_type_field
from .actions import PrintRoster, ApproveMembership, DenyMembership

from lino_medico.lib.medico.user_types import (SiteAdmin, SiteStaff, Physician,
                                OfficeStaff, get_user_type_from_role, UserTypes)
from lino_medico.lib.medico.exceptions import KeywordError
from lino.core import constants


# Company.Meta.verbose_name = _("Healthcare facility")
# Company.Meta.verbose_name_plural = _("Healthcare facilitys")


# class Partner(Partner, mixins.CreatedModified):
#
#     class Meta(Partner.Meta):
#         app_label = 'contacts'
#         # verbose_name = _("Partner")
#         # verbose_name_plural = _("Partners")
#         abstract = dd.is_abstract_model(__name__, 'Partner')
#
#     # isikukood = models.CharField(
#     #     _("isikukood"), max_length=20, blank=True)
#     #
#     hidden_columns = 'created modified'

    # faculty = None
    # """Required by :mod:`lino_xl.lib.working`.
    # """

    # def get_overview_elems(self, ar):
    #     # In the base classes, Partner must come first because
    #     # otherwise Django won't inherit `meta.verbose_name`. OTOH we
    #     # want to get the `get_overview_elems` from AddressOwner, not
    #     # from Partner (i.e. AddressLocation).
    #     elems = super(Partner, self).get_overview_elems(ar)
    #     elems += AddressOwner.get_overview_elems(self, ar)
    #     return elems


class PartnerDetail(PartnerDetail):

    main = "general #contact #invoicing #ledger #misc"

    general = dd.Panel("""
    overview:20 general2:20
    remarks:40 #sepa.AccountsByPartner
    # cal.AppointmentsByPartner
    # notes.NotesByPartner orders.OrdersByPartner
    """, label=_("General"))

    general2 = """
    id
    language
    #created
    #modified
    """

Partners.detail_layout = 'contacts.PartnerDetail'


dd.inject_field('contacts.Role', 'association_type', association_type_field)

def clean(self):
    super(Company, self).clean()
    if self.name != 'Lino Medico' and self.remarks == 'default':
        raise KeywordError("'default' is reserved for parent organization.")

Company.clean = clean

class Person(Person, SSIN):
    """
    Represents a patient (A normal user).
    """

    class Meta(Person.Meta):
        app_label = 'contacts'
        # verbose_name = _("Person")
        # verbose_name_plural = _("Persons")
        #~ ordering = ['last_name','first_name']
        abstract = dd.is_abstract_model(__name__, 'Person')

    print_roster = PrintRoster()

    @dd.displayfield(_("Print"))
    def print_actions(self, ar):
        if ar is None:
            return ''
        elems = [
            ar.instance_action_button(
                self.print_roster)]
        return E.p(*join_elems(elems, sep=", "))

    # @classmethod
    # def get_request_queryset(cls, *args, **kwargs):
    #     qs = super(Person, cls).get_request_queryset(*args, **kwargs)
    #     return qs.select_related('country', 'city')

    def get_print_language(self):
        "Used by DirectPrintAction"
        return self.language

    def cal_entries_by_guest(self):
        return rt.models.cal.Event.objects.filter(guest__partner=self)


# dd.update_field(Person, 'first_name', blank=False)
# dd.update_field(Person, 'last_name', blank=False)

# class PersonDetail(PersonDetail, PartnerDetail):
class PersonDetail(PartnerDetail):

    main = "general contact misc cal_tab"

    general = dd.Panel("""
    overview:20 general2:40 #general3:40
    # required_skill
    contacts.RolesByPerson:20
    """, label=_("General"))

    contact = dd.Panel("""
    # lists.MembershipsByPartner
    remarks:30 #sepa.AccountsByPartner
    """, label=_("Contact"))

    # humanlinks = dd.Panel("""
    # humanlinks.LinksByHuman:30
    # households.MembersByPerson:20 households.SiblingsByPerson:50
    # """, label=_("Human Links"))

    misc = dd.Panel("""
    url
    # created modified
    # notes.NotesByPartner
    """, label=_("Miscellaneous"))

    cal_tab = dd.Panel("""
    # print_actions
    #cal.GuestsByPartner cal.EntriesByGuest
    """, label=_("Calendar"))

    general2 = """
    title first_name:15 middle_name:15
    last_name
    gender:10 birth_date age:10
    id language
    """

    general3 = """
    email:40
    phone
    gsm
    fax
    """

    address_box = """
    country region city zip_code:10
    addr1
    street_prefix street:25 street_no street_box
    addr2
    """


Persons.insert_layout = """
first_name last_name
phone gsm
gender email
"""

# def get_persons_request_queryset(cls, ar, **filter):
#     calling_user = ar.get_user()
#     if calling_user.user_type == UserTypes.admin:
#         return super().get_request_queryset(ar, **filter)
#     elif calling_user.partner is not None:
#         return cls.objects.filter(pk=calling_user.partner.pk)
#
# setattr(Persons, 'get_request_queryset', classmethod(get_persons_request_queryset))

# class Company(Partner, Company):
#     class Meta(Company.Meta):
#         app_label = 'contacts'
#         abstract = dd.is_abstract_model(__name__, 'Company')

    # class Meta:
    #     verbose_name = _("Organisation")
    #     verbose_name_plural = _("Organisations")

    # vat_id = models.CharField(_("VAT id"), max_length=200, blank=True)


class CompanyDetail(PartnerDetail):

    main = "general contact #invoicing misc"

    # ledger = dd.Panel("""
    # vat.VouchersByPartner
    # ledger.MovementsByPartner
    # """, label=dd.plugins.ledger.verbose_name)

    general = dd.Panel("""
    overview:20 general2:40 general3:40
    contacts.RolesByCompany
    """, label=_("General"))

    general2 = """
    prefix:20 name:40
    type #vat_id
    url
    remarks
    """

    general3 = """
    email:40
    phone
    gsm
    fax
    """

    contact = dd.Panel("""
    # lists.MembershipsByPartner
    remarks:30 #sepa.AccountsByPartner
    """, label=_("Contact"))

    address_box = """
    country region city zip_code:10
    addr1
    street_prefix street:25 street_no street_box
    addr2
    """

    # tickets = "tickets.SponsorshipsByPartner"

    misc = dd.Panel("""
    id language
    # created modified
    # notes.NotesByPartner
    """, label=_("Miscellaneous"))

Companies.required_roles = dd.login_required(SiteAdmin)

Companies.insert_layout = """
name
phone gsm
#language:20 email:40
type #id
"""


class Patients(Persons):
    label = _("Patients")
    required_roles = dd.login_required(SiteAdmin)

class Worker(Person, Plannable):
    class Meta:
        app_label = 'contacts'
        verbose_name = _("Worker")
        verbose_name_plural = _("Workers")
        abstract = dd.is_abstract_model(__name__, 'Worker')

    specialties = ManyToManyField(
        "medico.Specialty", verbose_name=_("Medical specialties"))


    @dd.virtualfield(dd.ForeignKey('contacts.RoleType'))
    def role_type(self, ar):
        pass

    @dd.virtualfield(association_type_field)
    def association_type(self, ar):
        pass

    plannable_header_row_label = _("All workers")

    def get_actor(self, ar):
        calling_user = ar.get_user()
        if calling_user.partner is not None:
            try:
                calling_worker = self.__class__.objects.get(pk=calling_user.partner.pk)
                if calling_worker == self:
                    if calling_user.user_type == UserTypes.physician:
                        return rt.models.contacts.Physician
                    else:
                        return rt.models.contacts.Workers
            except self.__class__.DoesNotExist:
                pass

        worker_role = rt.models.contacts.Role.objects.filter(
            person=self.person, company=calling_user.association)
        if worker_role.exists():
            user_type = get_user_type_from_role(worker_role[0].type.name)
            if user_type == UserTypes.physician:
                if calling_user.user_type in [UserTypes.staff, UserTypes.admin]:
                    return rt.models.contacts.Physicians
                elif calling_user.user_type == UserTypes.physician:
                    return rt.models.contacts.Physician
            else:
                return rt.models.contacts.Workers


    def get_detail_action(self, ar):
        da = super().get_detail_action(ar)
        if da is not None:
            return da

        actor = self.get_actor(ar)
        if actor is not None:
            return actor.detail_action

    @classmethod
    def setup_parameters(cls, fields):
        fields.setdefault(
            'room', dd.ForeignKey('cal.Room',
                blank=True,
                help_text=_("Show only workers of this team.")))
        fields.update(role_type=dd.ForeignKey('contacts.RoleType', blank=True,
            help_text=_("Workers role type")))
        super(Worker, cls).setup_parameters(fields)

    @classmethod
    def facility_specific_query(cls, ar, qs):
        pv = ar.param_values

        role_filter=dict(person__pk=OuterRef('pk'))
        user_filter=dict(partner__person__pk=OuterRef('pk'))

        if hasattr(pv, 'role_type') and pv.role_type:
            user_filter.update(user_type=get_user_type_from_role(pv.role_type.name))
            role_filter.update(type=pv.role_type)

        # if pv.room:
        #     qs = qs.filter(team_memberships__room=pv.room)

        default_match = False

        company = ar.get_user().association
        if company is not None and company.remarks != 'default':
            role_filter.update(company=company)
            user_filter.update(association=company)
        else:
            default_match = True

        physician = rt.models.contacts.RoleType.objects.get(name='Physician')

        physician_filter = copy.copy(role_filter)
        physician_filter.update(type=physician)

        return qs.annotate(
            match_found=Case(
                When(Exists(rt.models.contacts.Role.objects.filter(
                    **role_filter
                )), then=Value(True)),
                When(Exists(rt.models.users.User.objects.filter(
                    **user_filter
                )), then=Value(True)),
                default=Value(default_match),
                output_field=BooleanField()
            )
        ).filter(match_found=True).annotate(physician=Case(
            When(Exists(rt.models.contacts.Role.objects.filter(
                **physician_filter
            )), then=Value(True)),
            default=Value(False),
            output_field=BooleanField()
        ))

    @classmethod
    def get_request_queryset(cls, ar, **filter):
        calling_user = ar.get_user()
        if calling_user.user_type in [UserTypes.staff, UserTypes.admin]:
            return cls.facility_specific_query(ar, super().get_request_queryset(ar, **filter))
        elif calling_user.partner is not None:
            return cls.objects.filter(pk=calling_user.partner.pk)

    @classmethod
    def get_title_tags(self, ar):
        for t in super(Worker, self).get_title_tags(ar):
            yield t
        pv = ar.param_values
        if pv.room:
            yield str(pv.room)

    def get_weekly_chunks(self, ar, entries, today):
        sums = SumCollector()
        for e in entries:  # .filter(guest__partner=self).distinct():
            yield e.as_summary_item(ar, ar.actor.get_calview_div(e, ar))
            sums.collect(e.event_type, e.get_duration())
        for k, v in sums.items():
            yield _("{} : {}").format(k, str(v))
            # need to explicitly call str(v) because otherwise __format__() is
            # used, which would render it like a Decimal

    @classmethod
    def get_plannable_entries(cls, obj, qs, ar):

        # The header row in a workers calendar view shows entries that
        # are for everybody, e.g. holidays.  This is when
        # cal.EventType.locks_user is True.
        # print("20200303 get_plannable_entries", cls, obj, ar)
        # Event = rt.models.cal.Event
        if obj is None:
            return qs.none()
        User = rt.models.users.User
        # qs = Event.objects.all()
        if obj is cls.HEADER_ROW:
            # qs = qs.filter(event_type__locks_user=False)
            qs = qs.filter(event_type__all_rooms=True)
        else:
            # entries where the worker is either a guest or the author
            # qs = qs.filter(event_type__locks_user=True)
            try:
                u = User.objects.get(partner=obj)
                # print(u)
                qs = qs.filter(Q(user=u) | Q(guest__partner=obj)).distinct()
            except Exception:
                qs = qs.filter(guest__partner=obj).distinct()
        return qs
        # return Event.calendar_param_filter(qs, ar.param_values)


class WorkerDetail(PersonDetail):

    main = "general contact misc cal_tab"

    general = dd.Panel("""
    overview:20 general2:40 #general3:40
    contacts.RolesByPerson:20
    """, label=_("General"))

    contact = dd.Panel("""
    first_name middle_name last_name
    phone gsm fax
    email url
    gender birth_date
    national_id birth_certificate_no nationality
    """, label=_("Contact"))

    misc = dd.Panel("""
    url
    # created modified
    # notes.NotesByPartner
    """, label=_("Miscellaneous"))

    cal_tab = dd.Panel("""
    # print_actions
    #cal.GuestsByPartner cal.EntriesByGuest
    """, label=_("Calendar"))

    general2 = """
    title first_name:15 middle_name:15
    last_name
    gender:10 birth_date age:10
    id language
    """

    general3 = """
    email:40
    phone
    gsm
    fax
    """

    address_box = """
    country region city zip_code:10
    addr1
    street_prefix street:25 street_no street_box
    addr2
    """

class PhysicianDetail(WorkerDetail):

    general = dd.Panel("""
    overview:20 general2:40 #general3:40
    medico.SpecialtiesByPhysician
    contacts.RolesByPerson:20
    """, label=_("General"))

class BaseWorkers(dd.Table):
    model = 'contacts.Worker'
    detail_layout = 'contacts.WorkerDetail'
    required_roles = dd.login_required(SiteAdmin)

class Workers(BaseWorkers):
    label = _("Workers")
    required_roles = dd.login_required((SiteStaff, OfficeStaff, SiteAdmin))
    # detail_layout = WorkerDetail()
    params_layout = 'role_type room gender observed_event start_date end_date'

    insert_layout = """
    first_name middle_name last_name
    phone gsm
    gender email
    association_type role_type
    national_id birth_certificate_no nationality
    """

    @classmethod
    def after_create_instance(self, obj, ar):
        super().after_create_instance(obj, ar)
        calling_user = ar.get_user()
        if calling_user.association is None:
            return
        try:
            role_type = rt.models.contacts.RoleType.objects.get(pk=ar.rqdata['role_typeHidden'])
            role, _ = rt.models.contacts.Role.objects.get_or_create(person=obj, company=calling_user.association)
            need_save = False
            if role.type != role_type:
                role.type = role_type
                need_save = True
            at = ar.rqdata['association_typeHidden']
            if at is not None:
                role.association_type = at
                need_save = True
            if need_save:
                role.full_clean()
                role.save()
        except ValueError:
            pass

    @classmethod
    def get_request_queryset(cls, ar, **filter):
        return super().get_request_queryset(ar, **filter).filter(physician=False)


class Physician(BaseWorkers):
    required_roles = dd.login_required(Physician)
    detail_layout = 'contacts.PhysicianDetail'
    params_layout = 'association_type room gender observed_event start_date end_date'

    # @classmethod
    # def get_request_queryset(cls, ar, **filter):
    #     u = ar.get_user()
    #     assert u.user_type == UserTypes.physician
    #     return rt.models.contacts.Worker.objects.filter(
    #         pk=ar.get_user().partner.person.pk
    #     )

class Physicians(BaseWorkers):
    label = _("Physicians")
    required_roles = dd.login_required((SiteAdmin, SiteStaff))
    params_layout = 'association_type room gender observed_event start_date end_date'

    detail_layout = 'contacts.PhysicianDetail'

    insert_layout = """
    first_name middle_name last_name
    phone gsm
    gender email
    association_type
    national_id birth_certificate_no nationality
    """

    @classmethod
    def after_create_instance(self, obj, ar):
        super().after_create_instance(obj, ar)
        calling_user = ar.get_user()
        if calling_user.association is None:
            return
        role_type = rt.models.contacts.RoleType.objects.get(name="Physician")
        role, created = rt.models.contacts.Role.objects.get_or_create(person=obj, company=calling_user.association)
        need_save = created
        if role.type != role_type:
            role.type = role_type
            need_save = True
        at = ar.rqdata['association_typeHidden']
        if at is not None:
            role.association_type = at
            need_save = True
        if need_save:
            role.full_clean()
            role.save()

    @classmethod
    def get_request_queryset(cls, ar, **filter):
        return super().get_request_queryset(ar, **filter).filter(physician=True)


class RoleTypes(RoleTypes):
    insert_layout = """
    name can_sign
    """

CompanyTypes.required_roles = dd.login_required(SiteAdmin)

# class WorkersParameters(ParameterClone):
class WorkersParameters(ParameterClone):

    abstract = True
    # clone_from = "contacts.Workers"

    @classmethod
    def get_dayslave_rows(cls, ar):
        return rt.models.contacts.Worker.objects.all()

    @classmethod
    def unused_get_calview_chunks(cls, obj, ar):
        pv = ar.param_values
        # if pv.user:
        # if pv.assigned_to:
        # if settings.SITE.project_model is not None and pv.project:
        # if pv.event_type:
        if obj.start_time:
            yield str(obj.start_time)[:5] + " "
        # elif not pv.start_date:
            # t.append(str(self.start_date))
        # if not pv.user and self.user:
        #     t.append(str(self.user))
        if obj.project:
            yield str(obj.project) + " "
            if obj.project.city:
                yield obj.project.city.name + " "
        # if not pv.event_type and self.event_type:
        #     t.append(str(self.event_type))
        if obj.room and obj.room.ref:
            yield obj.room.ref + " "
        if obj.summary:
            yield obj.summary + " "

    def get_header_chunks(obj, ar, entries, today):

        # unlike the library version, this does not have an insert button and
        # does not show only whole-day events.

        # entries = entries.filter(start_time__isnull=True)
        txt = str(today.day)
        if today == dd.today():
            txt = E.b(txt)

        yield E.p(txt, align="center")
        for e in entries:
            yield e.as_summary_item(ar, ar.actor.get_calview_div(e, ar))

    @dd.displayfield(_("Worker"))
    def name_column(cls, obj, ar):
        # x1 = str([o.as_summary_item(ar) for o in ar.selected_rows])
        d = ar.master_instance.date
        d -= datetime.timedelta(days=d.weekday())  # start at first day of week
        ba = obj.print_roster
        if ba is None:
            return E.p(obj.as_summary_item(ar))

        pv = dict(start_date=d, end_date=d + datetime.timedelta(days=6))
        # print("20200417", pv)
        # lbl = "🖨" # unicode 1f5a8 printer
        lbl = "🖶" # unicode 1f5b6 printer icon
        btn = ar.instance_action_button(ba, lbl,
            request_kwargs=dict(action_param_values=pv))
        return E.p(obj.as_summary_item(ar), " ", btn)


class WeeklySlave(WorkersParameters, WeeklySlaveBase, Workers):
# 20200430 class WeeklySlave(Workers, WeeklySlaveBase):
    column_names_template = "name_column:20 {vcolumns}"
    hide_navigator = False  # why?
    # hide_top_toolbar = False


class DailySlave(WorkersParameters, DailySlaveBase, Workers):
# 20200430 class DailySlave(Workers, DailySlaveBase):
    column_names_template = "name_column:20 {vcolumns}"
    # display_mode = ((None, constants.DISPLAY_MODE_HTML), )
    navigation_mode = 'day'

class WeeklyView(WorkersParameters, CalendarView):
    label = _("Weekly view")
    detail_layout = 'contacts.WeekDetail'
    navigation_mode = "week"
    # params_layout = ""

class DailyView(WorkersParameters, CalendarView):
    label = _("Daily view")
    detail_layout = 'contacts.DayDetail'
    navigation_mode = "day"
    insert_event = InsertEvent()

class WeekDetail(dd.DetailLayout):
    main = "body"
    body = "navigation_panel:15 contacts.WeeklySlave:85"

class DayDetail(dd.DetailLayout):
    main = "body"
    body = "navigation_panel:15 contacts.DailySlave:85"



add = Planners.add_item
add("contacts", _("Workers planner"), "contacts.DailyView", "contacts.WeeklyView", "")



class MembershipRequest(UserAuthored):

    class Meta:
        abstract = dd.is_abstract_model(__name__, 'MembershipRequest')


    association = ForeignKey('contacts.Company',
        verbose_name=_("Healthcare facility"), on_delete=CASCADE)
    role_type = ForeignKey('contacts.RoleType', on_delete=CASCADE)
    association_type = association_type_field

    approve_membership = ApproveMembership()
    deny_membership = DenyMembership()

    # preferred_username = dd.CharField(_("Preferred username"), max_length=80, default="", blank=True)
    # preferred_password = dd.CharField(_("Preferred password"), max_length=80, default="", blank=True)
    # keep_old_password = False
    # keep_old_password_field = dd.VirtualBooleanField('keep_old_password', _("Keep current user password"))
    #
    # def clean(self):
    #     super().clean()
    #     if (
    #         self.preferred_username != "" and
    #         rt.models.users.User.objects.filter(username=self.preferred_username).exists()
    #     ):
    #         raise Exception(_("The username {} is taken. "
    #                 "Please choose another one").format(self.preferred_username))
    #     if self.keep_old_password:
    #         self.preferred_password = ""
    #     elif self.preferred_password == "":
    #         raise Exception(_("Please Provide a valid password"))




class AllowMembershipRequests(dd.Table):
    required_roles = set()
    model = MembershipRequest
    no_phantom_row = True

    insert_layout = """
    association
    role_type association_type #preferred_username
    # preferred_password keep_old_password_field
    """

class MembershipRequests(AllowMembershipRequests):
    required_roles = dd.login_required(OfficeStaff)
    label = _("Membership Requests")
    column_names = "user association role_type association_type"

    # detail_layout = """
    # user__first_name user__last_name
    # user__user_type
    # association association__name"""

    @classmethod
    def get_request_queryset(cls, ar):
        org = ar.get_user().association
        qs = super().get_request_queryset(ar)
        qs = qs.filter(association=org)
        return qs

    @classmethod
    def collect_extra_actions(cls):
        cls.insert_action.action.show_in_toolbar = False
        return []
