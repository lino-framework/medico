from django.conf import settings
from lino_xl.lib.contacts.fixtures.demo import *
from lino_medico.lib.medico.user_types import get_user_type_from_role

objs = objects

def objects():
    yield objs()

    cumilla = lookup_city('Cumilla')

    User = settings.SITE.user_model

    Worker = rt.models.contacts.Worker
    RoleType = rt.models.contacts.RoleType
    AT = rt.models.contacts.AssociationTypes

    lgh = company(
        'Laksham General Hospital', '3570', cumilla, 'Laksham', 'Laksham')
    yield lgh

    def worker(first_name, last_name, gender, role_type, association_type):
        w = Worker(city=cumilla, first_name=first_name, last_name=last_name, gender=gender)
        yield w
        yield rt.models.contacts.Role(
            company=lgh,
            person=w,
            type=role_type,
            association_type=association_type
        )
        user_type = get_user_type_from_role(role_type.name)
        if user_type is not None:
            user_kw = dict(
                user_type=user_type,
                email=settings.SITE.demo_email,
                language=settings.SITE.language_dict['en'].django_code,
                first_name=w.first_name,
                last_name=w.last_name,
                partner=w,
                association=lgh
            )
            user_kw.update(username=user_kw.get('first_name').lower())
            yield User(**user_kw)


    yield worker('Parvez', 'Ahmed', dd.Genders.male,
        RoleType.objects.get(name='Office staff'), AT.get_by_name('indoor')
    )
    yield worker('Faruk', 'Hossain', dd.Genders.male,
        RoleType.objects.get(name='Reception clerk'), AT.get_by_name('indoor')
    )
    yield worker('Nusrat', 'Ritu', dd.Genders.female,
        RoleType.objects.get(name='Nurse'), AT.get_by_name('indoor')
    )
    yield worker('Nazir', 'Ahmed', dd.Genders.male,
        RoleType.objects.get(name='Physician'), AT.get_by_name('indoor')
    )

    yield worker('Salma', 'Begum', dd.Genders.female,
        RoleType.objects.get(name='Physician'), AT.get_by_name('outdoor')
    )
