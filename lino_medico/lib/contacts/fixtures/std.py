# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import _
from lino_xl.lib.contacts.fixtures.std import *

objs = objects

def objects():
    yield objs()

    Company = rt.models.contacts.Company
    companyType = rt.models.contacts.CompanyType
    roletype = rt.models.contacts.RoleType


    companyType.objects.all().delete()
    # Company.objects.all().delete()

    yield Company(
        email='sharifmehedi24@gmail.com',
        language='en',
        name='Lino Medico',
        remarks='default'
    )

    ORG_TYPES = """\
Ambulance Service
Organ and/or Blood bank
Clinic and medical office
Hospital
Imaging and radiology center
Pathological center
Mental health and addiction treatment center
Nursing home
Emergency care
Urgent care""".splitlines()

    for name in ORG_TYPES:
        yield companyType(name=name)

    ROLE_TYPES = """\
Reception clerk
Nurse
Supervisor
Cleaner""".splitlines()

    yield roletype(name=str(_("Physician")), can_sign=True)
    yield roletype(name=str(_("Office staff")), can_sign=True)

    for role_type in ROLE_TYPES:
        yield roletype(name=str(_(role_type)))

    yield rt.models.cal.GuestRole(ref='patient', name=_('Patient'))
