# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from django.conf import settings
from lino.api import rt, _
from lino.utils import mti

from lino_medico.lib.medico.user_types import UserTypes

def objects():
    Company = rt.models.contacts.Company
    Country = rt.models.countries.Country

    default_company = Company.objects.get(remarks='default')
    default_company.country = Country.objects.get(isocode='BD')
    yield default_company

    settings.SITE.site_config.update(site_company=default_company)

    demo_users = settings.SITE.demo_users
    User = rt.models.users.User
    Worker = rt.models.contacts.Worker

    for user in User.objects.filter(username__in=demo_users):
        if user.user_type != UserTypes.user:
            mti.insert_child(user.partner.person, Worker, full_clean=True)
