# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import dd, _
from lino_xl.lib.contacts.choicelists import *
# from lino_medico.lib.medico.user_types import OfficeStaff, SiteStaff, SiteAdmin

class AssociationTypes(dd.ChoiceList):
    verbose_name = _("Association type")
    verbose_name_plural = _("Association types")
    max_length = 3
    # required_roles = dd.login_required(dd.SiteStaff)

add = AssociationTypes.add_item

add('10', _("Indoor"), 'indoor')
add('20', _("Outdoor"), 'outdoor')
add('30', _("Combined"), 'combined')

association_type_field = AssociationTypes.field(
    default='combined',
    help_text=_("Involement of the worker with the healthcare facility"))
