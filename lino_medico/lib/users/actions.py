# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import dd, _
from lino.modlib.users.actions import *
from lino.modlib.users.actions import CreateAccount as BaseCreateAccount
from lino.utils import mti

from lino_medico.lib.medico.user_types import UserTypes

parameters = BaseCreateAccount.parameters
# parameters.update(create_patient=models.BooleanField(_("Create patient profile"), default=False))
parameters.update(national_id=dd.CharField(_("National Identification Number (*)"), default=""))

class CreateAccount(BaseCreateAccount):

    parameters = parameters
    params_layout = BaseCreateAccount.params_layout + "national_id"

    def run_from_ui(self, ar, **kwargs):
        pv = ar.action_param_values
        national_id = pv.pop('national_id')
        # create_patient = pv.pop('create_patient')
        # if create_patient:
        if not national_id:
            raise Exception(_("UnknownValue SSIN/NID: Cannot create patient profile."))
        # else:
        #     if SSIN:
        #         raise Exception(_("Ignoring SSIN/NID: Create patient profile set to False."))

        super().run_from_ui(ar, **kwargs)

        # if create_patient:
        username = pv.pop('username')
        pv.pop('password')
        pv.update(national_id=national_id)
        patient = rt.models.contacts.Person(**pv)
        patient.full_clean()
        patient.save()

        user = rt.models.users.User.objects.get(username=username)
        user.partner = patient
        user.verification_code = "" # SET OFF verification process
        user.full_clean()
        user.save()

class MySettings(MySettings):
    label = _("Profile & Preferences")

    def run_from_ui(self, ar, **kw):
        user = ar.get_user()
        Worker = rt.models.contacts.Worker
        if user.user_type == UserTypes.user:
            da = rt.models.users.Me.detail_action
        else:
            if user.user_type == UserTypes.physician:
                da = rt.models.users.MeAsPhysician.detail_action
            else:
                da = rt.models.users.MeAsWorker.detail_action
        ar.goto_instance(user, da)
