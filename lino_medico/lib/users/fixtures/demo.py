# -*- coding: UTF-8 -*-
# Copyright 2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from django.conf import settings

from lino.api import rt, _
from lino.modlib.users.fixtures.demo import *
from lino_medico.lib.medico.user_types import UserTypes

objs = objects

settings.SITE.demo_users = []

def objects():
    yield objs()

    User = settings.SITE.user_model
    Patient = rt.models.contacts.Person

    def create_user(first_name, last_name, language, user_type, username):
        settings.SITE.demo_users.append(username)
        user = User(first_name=first_name, last_name=last_name, language=language, user_type=user_type, username=username)
        patient = Patient(first_name=first_name, last_name=last_name, language=language)
        yield patient
        user.partner = patient
        yield user

    yield create_user(first_name='Agnes', last_name='Obel', language='en', user_type=UserTypes.staff, username='agnes')
    yield create_user(first_name='Dan', last_name='Reynolds', language='en', user_type=UserTypes.officestaff, username='reynolds')
    yield create_user(first_name='Olafur', last_name='Arnalds', language='en', user_type=UserTypes.physician, username='olafur')
    yield create_user(first_name='Michael', last_name="O'Donnell", language='en', user_type=UserTypes.receptionclerk, username='michael')
    yield create_user(first_name='Carlos', last_name='Zafon', language='en', user_type=UserTypes.user, username='carlos')
