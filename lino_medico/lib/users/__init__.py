# -*- coding: UTF-8 -*-
# Copyright 2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)


"""
Lino Medico extension of :mod:`lino.modlib.users`.

.. autosummary::
   :toctree:

    models
    ui
    fixtures.demo
    fixtures.demo2

"""

from lino.api import _
from lino.modlib.users import Plugin


class Plugin(Plugin):

    extends_models = ['User']

    my_setting_text = _("Profile & Preferences")

    def setup_config_menu(self, site, user_type, m):
        # super().setup_config_menu(site, user_type, m)
        g = site.plugins.system
        m = m.add_menu(g.app_label, g.verbose_name)
        m.add_action('users.AllUsers')
        m.add_action('users.OurUsers')
