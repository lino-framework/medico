# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""Desktop UI for this plugin.

"""

from lino.api import dd, _
from lino.utils.html import E, tostring

from lino.modlib.users.ui import *

from lino_medico.lib.medico.user_types import SiteStaff, SiteAdmin, UserTypes


class UserDetail(UserDetail):
    """Layout of User Detail in Lino Medico."""

    main = "general contact #patient"

    general = dd.Panel("""
    box1
    remarks:40 users.AuthoritiesGiven:20
    """, label=_("General"))

    box1 = """
    username user_type:20
    language time_zone
    id created modified
    """

    contact = dd.Panel("""
    first_name pp__middle_name last_name initials
    partner__phone partner__gsm partner__fax
    email partner__url
    pp__gender pp__birth_date
    pp__national_id pp__nationality
    partner
    """.replace('pp__', 'partner__person__'), label=_("Contact"))


    # patient = dd.Panel("""
    # , label=_("Patient profile"))

class WorkerUserDetail(UserDetail):

    main = UserDetail.main + ' employment'

    employment = dd.Panel("""
    association
    #specialties
    """, label=_("Employment"))

class PhysicianUserDetail(WorkerUserDetail):

    employment = dd.Panel("""
    association
    medico.SpecialtiesByUser
    #specialties
    """, label=_("Employment"))

Users.detail_layout = UserDetail()
Users.column_names = "first_name email #place #offered_products #wanted_products"

class OurUsers(AllUsers):
    label = _("Our users")
    required_roles = dd.login_required(SiteStaff)

    insert_layout = """
    username email
    first_name last_name
    partner
    user_type
    """

    @classmethod
    def get_request_queryset(cls, ar):
        qs = super().get_request_queryset(ar)
        return qs.filter(association=ar.get_user().association)

    @classmethod
    def create_instance(cls, ar, **kw):
        calling_user = ar.get_user()
        kw.update(
            association=calling_user.association,
            language = calling_user.language,
            time_zone = calling_user.time_zone,
            verification_code = ""
        )
        return super().create_instance(ar, **kw)

class Me(Me):

    label = _("Profile & Preferences")

    @classmethod
    def collect_extra_actions(cls):
        yield dd.WrappedAction(
            rt.models.contacts.AllowMembershipRequests.insert_action,
            label=_("Request membership"),
            icon_name=None)


class MeAsWorker(Me):
    detail_layout = 'users.WorkerUserDetail'

class MeAsPhysician(MeAsWorker):
    detail_layout = 'users.PhysicianUserDetail'
