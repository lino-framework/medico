# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""Database models for this plugin.

"""

from django.db.models import Case, When, Exists, Value, BooleanField, OuterRef

from lino.api import rt, dd, _
from lino.modlib.users.models import *
from lino.utils import mti

from lino_medico.lib.medico.user_types import SiteAdmin, SiteStaff

from .actions import CreateAccount, MySettings

class User(User):

    # healthcare facility
    association = models.ForeignKey('contacts.Company', on_delete=models.SET_NULL, null=True, blank=True)

    class Meta(User.Meta):
        abstract = dd.is_abstract_model(__name__, 'User')

    @dd.chooser()
    def partner_choices(cls, ar):
        Worker = rt.models.contacts.Worker
        return Worker.facility_specific_query(ar, Worker.objects.all())


    my_settings = MySettings()

    @property
    def worker(self):
        if self.partner is not None:
            return mti.get_child(self.partner, rt.models.contacts.Worker)

    def disabled_fields(self, ar):
        """
        Only System admins may change the `user_type` of users.
        See also :meth:`Users.get_row_permission`.
        """
        rv = super(User, self).disabled_fields(ar)
        user = ar.get_user()
        if user.user_type.has_required_roles([SiteStaff]):
            if 'user_type' in rv: rv.remove('user_type')
            if 'partner' in rv: rv.remove('partner')
            if 'send_email' in rv: rv.remove('send_email')
            # merging two users is only for SiteAdmin, even when you are Expert
            if 'merge_row' in rv: rv.remove('merge_row')
            if user != self:
                if 'change_password' in rv: rv.remove('change_password')
        return rv

    def get_row_permission(self, ar, state, ba):
        if not ba.action.readonly:
            user = ar.get_user()
            if user != self:
                if user.user_type.has_required_roles([SiteStaff]):
                    return True
        return super().get_row_permission(ar, state, ba)


if dd.plugins.users.allow_online_registration:
    About.create_account = CreateAccount()


from .ui import *
