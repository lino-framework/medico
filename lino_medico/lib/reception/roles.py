# -*- coding: UTF-8 -*-
# Copyright 2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.core.roles import UserRole

class ReceptionUser(UserRole):
    pass

class ReceptionOperator(UserRole):
    pass

class ReceptionStaff(ReceptionUser, ReceptionOperator):
    pass
