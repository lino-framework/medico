# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino_xl.lib.reception.models import *
from lino_xl.lib.cal.workflows.feedback import *
from lino.api import _, rt, dd
from lino.core import constants

from .roles import ReceptionStaff, ReceptionOperator, ReceptionUser

GuestStates.clear()
add = GuestStates.add_item
add('10', _("Invited"), 'invited')
add('40', _("Present"), 'present', afterwards=True)
add('44', _("Waiting"), 'waiting')
add('45', _("Busy"), 'busy')
add('46', _("Gone"), 'gone')
add('50', _("Absent"), 'missing', afterwards=True)
add('60', _("Excused"), 'excused', afterwards=True)

GuestStates.present_states = set([
    GuestStates.present,
    GuestStates.waiting, GuestStates.busy, GuestStates.gone ])

GuestStates.present.add_transition(MarkPresent)
GuestStates.missing.add_transition(MarkAbsent)
GuestStates.excused.add_transition(MarkExcused)

class WaitingVisitors(WaitingVisitors):
    column_names = "position since workflow_buttons partner"

    @dd.displayfield(_('Since'))
    def since(self, obj, ar=None):
        if ar is not None:
            return obj.event.as_summary_item(ar, naturaltime(obj.waiting_since))

    @dd.displayfield(
        _('Position'), help_text=_("Position in waiting queue (per agent)"))
    def position(self, obj, ar=None):
        n = 1 + rt.models.cal.Guest.objects.filter(
            #~ waiting_since__isnull=False,
            #~ busy_since__isnull=True,
            state=GuestStates.waiting,
            event__user=obj.event.user,
            waiting_since__lt=obj.waiting_since).count()
        return str(n)

class ReceiveVisitor(ReceiveVisitor):

    def run_from_ui(self, ar, **kw):
        obj = ar.selected_rows[0]

        def ok(ar):
            obj.state = GuestStates.busy
            obj.busy_since = timezone.now()

            if not obj.event.start_time:
                ar.info("event.start_time has been set")
                obj.event.start_time = obj.busy_since
                obj.event.save()

            slot = obj.event.slots.get(
                enrolment__isnull=False,
                enrolment__pupil=obj.partner
            )
            if slot.enrolment.course is None:
                course = rt.models.courses.Course(
                    user=rt.models.users.User.objects.get(
                        association=obj.event.line.first().company,
                        partner=obj.event.line.first().contact_person
                    ),
                    line=obj.event.line.first(),
                    teacher=obj.event.line.first().contact_person.worker,
                    pupil=obj.partner
                )
                course.save_new_instance(ar)
                prescription = rt.models.courses.Prescription(course=course)
                prescription.save_new_instance(ar)

            obj.save()
            ar.success()
            # ar.success(refresh=True)
            ar.goto_instance(prescription)

        ar.confirm(ok,
                   _("%(guest)s begins consultation with %(user)s.") %
                   dict(user=obj.event.user, guest=obj.partner),
                   _("Are you sure?"))


dd.update_model('cal.Guest', receive=ReceiveVisitor(sort_index=101))

# class PublishEvent(PublishEvent):
#
#     def run_from_ui(self, ar, **kw):
#         super().run_from_ui(ar, **kw)
#         obj = ar.selected_rows[0]
#         if obj.has_child_entries:
#             obj.update_children()
#
# EntryStates.published.add_transition(PublishEvent)
