
from lino.api import rt


def make_room(name, company, contact_person, **kwargs):
    return rt.models.cal.Room(
        name=name, company=company, contact_person=contact_person, **kwargs)

def objects():
    lgh = rt.models.contacts.Company.objects.filter(
        name='Laksham General Hospital').first()
    parvez = rt.models.contacts.Person.objects.filter(
        first_name='Parvez').first()
    yield make_room('101', lgh, parvez)
