# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import dd, _
from lino_xl.lib.products.choicelists import *

add = ProductTypes.add_item
add('200', _("Test"), 'test', table_name='products.Tests')
add('300', _("Appointment"), 'appointment', table_name='products.Appointments')
add('400', _("Transportation"), 'transportation')
add('500', _("Surgery"), 'surgery')
add('600', _("Bed"), 'bed')
add('700', _("Medicine"), 'medicine', table_name='products.Medicines')
add('800', _("Med utility kit"), 'medkit')

class MedicineTypes(dd.ChoiceList):
    pass

add = MedicineTypes.add_item
add('100', _("Tablet"), 'tablet')
add('200', _("Capsule"), 'capsule')
add('300', _("Syrup"), 'syrup')
add('400', _("Injectable"), 'injectable')
add('500', _("Saline"), 'saline')
add('600', _("Injectable saline"), 'injectable_saline')
add('700', _("Injectable through saline"), 'injectable_thourgh_saline')
