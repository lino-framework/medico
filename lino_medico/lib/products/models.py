# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from django.db import models
from django.core.exceptions import ValidationError

from lino.api import rt, dd, _
from lino_xl.lib.products.models import *
from lino_xl.lib.measurements.mixins import *

from lino_medico.lib.medico.user_types import SiteStaff, SiteAdmin, Physician

from .choicelists import ProductTypes, MedicineTypes


class Category(Category):
    class Meta(Category.Meta):
        abstract = dd.is_abstract_model(__name__, 'Category')

    def before_ui_save(self, ar, cw):
        super().before_ui_save(ar, cw)
        if not ar.rqdata.get('parent', False):
            raise ValidationError("parent field cannot be blank.")

    def clean(self):
        super().clean()
        if self.parent:
            self.product_type = self.parent.product_type

Categories.insert_layout = """
name
parent
body
"""

class Product(Product):

    class Meta(Product.Meta):
        abstract = dd.is_abstract_model(__name__, 'Product')

    vendor = dd.ForeignKey('contacts.Company', related_name='products_by_vendor', null=True, blank=True)
    seller = dd.ForeignKey('contacts.Partner', related_name='products_by_seller', null=True, blank=True)

    def before_ui_save(self, ar, cw):
        super().before_ui_save(ar, cw)
        if self.vendor is None:
            self.vendor = ar.get_user().association

    @classmethod
    def get_request_queryset(cls, ar, **filter):
        return super().get_request_queryset(ar, **filter).filter(
            vendor=ar.get_user().association
        )

BaseProducts.required_roles = dd.login_required((SiteStaff, SiteAdmin))

class Medicine(Product, Measurable):
    unit = Measurable.units(['weight', 'volume']).field(default='weightunit_mg')

    class Meta(Product.Meta):
        verbose_name = _("Medicine")
        verbose_name_plural = _("Medicine")
        abstract = dd.is_abstract_model(__name__, 'Medicine')

    type = MedicineTypes.field(null=True, blank=True)
    administration_route = dd.ForeignKey('courses.AdministrationRoute', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        med = super().__str__()
        if self.measurement is not None:
            return med + " " + str(self.m)
        return med

    def before_ui_save(self, ar, cw):
        super().before_ui_save(ar, cw)
        if ar.rqdata.get('vendor', "") == "":
            self.vendor = None

    @classmethod
    def get_request_queryset(cls, ar, **filter):
        return cls.objects.filter(**filter)

dd.update_field('products.Medicine', 'product_type', default=ProductTypes.medicine)

@dd.receiver(dd.pre_save, sender='products.Medicine')
def duplicate(sender, instance, **kwargs):
    if instance.id is not None:
        existent = sender.objects.get(pk=instance.pk)
        likes = sender.objects.filter(name=instance.name)
        found = False
        for obj in likes:
            if obj.m == instance.m and obj.id != instance.id:
                like = obj
                found = True
                break
        if found:
            raise ValidationError("Alike object already exists!")
        if instance.m != existent.m:
            existent.id = None
            existent.full_clean()
            existent.save()

class Medicines(dd.Table):
    label = _("Medicine")
    model = 'products.Medicine'
    required_roles = dd.login_required((SiteStaff, SiteAdmin, Physician))
    column_names = 'name category product_type vendor sales_price'

class Test(Product):

    class Meta:
        app_label = 'products'
        verbose_name = _('Test')
        verbose_name_plural = _('Tests')
        abstract = dd.is_abstract_model(__name__, 'Test')

class Tests(dd.Table):
    model = 'products.Test'
    required_roles = dd.login_required((SiteStaff, ))

class Appointments(BaseProducts):
    label = _("Appointments")
