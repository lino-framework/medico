# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import rt, dd, _

def objects():
    productcat = rt.models.products.Category
    producttypes = rt.models.products.ProductTypes

    test = productcat(name=_("Test"), product_type=producttypes.test)
    yield test
    yield productcat(name=_("Appointment"), product_type=producttypes.appointment)
    yield productcat(name=_("Transportation"), product_type=producttypes.transportation)
    yield productcat(name=_("Surgery"), product_type=producttypes.surgery)
    yield productcat(name=_("Bed"), product_type=producttypes.bed)
    yield productcat(name=_("Med utility kit"), product_type=producttypes.medkit)
    drug = productcat(name=_("Medicine"), product_type=producttypes.medicine)
    yield drug

    # source https://www.fda.gov/drugs/investigational-new-drug-ind-application/general-drug-categories

    analgesics = productcat(name=_('Analgesics'), parent=drug, body=_("Drugs that relieve pain."))
    yield analgesics
    yield productcat(name=_("Analgesics (non-narcotic)"), parent=analgesics, body=_("Used to relieve mild pain."))
    yield productcat(name=_("Analgesics (narcotic)"), parent=analgesics, body=_("Used to relieve severe pain."))

    yield productcat(name=_('Antacids'), parent=drug, body=_("Drugs that relieve indigestion and heartburn by neutralizing stomach acid."))
    yield productcat(name=_('Antianxiety Drugs'), parent=drug, body=_("Drugs that suppress anxiety and relax muscles (sometimes called anxiolytics, sedatives, or minor tranquilizers)."))
    yield productcat(name=_('Antiarrhythmics'), parent=drug, body=_("Drugs used to control irregularities of heartbeat."))
    yield productcat(name=_('Antibacterials'), parent=drug, body=_("Drugs used to treat infections."))
    yield productcat(name=_('Antibiotics'), parent=drug, body=_("Drugs made from naturally occurring and synthetic substances that combat bacterial infection. Some antibiotics are effective only against limited types of bacteria. Others, known as broad spectrum antibiotics, are effective against a wide range of bacteria."))
    yield productcat(name=_('Anticoagulants and Thrombolytics'), parent=drug, body=_("Anticoagulants prevent blood from clotting. Thrombolytics help dissolve and disperse blood clots and may be prescribed for patients with recent arterial or venous thrombosis."))
    yield productcat(name=_('Anticonvulsants'), parent=drug, body=_("Drugs that prevent epileptic seizures."))

    antidepressants = productcat(name=_('Antidepressants'), parent=drug, body=_("There are three main groups of mood-lifting antidepressants: tricyclics, monoamine oxidase inhibitors, and selective serotonin reuptake inhibitors (SSRIs)."))
    yield antidepressants
    yield productcat(name=_('Antidepressants (tricyclics)'), parent=antidepressants, body=_("Mood lifting drug."))
    yield productcat(name=_('Antidepressants (monoamine oxidase inhibitors)'), parent=antidepressants, body=_("Mood lifting drug."))
    yield productcat(name=_('Antidepressants (selective serotonin reuptake inhibitors (SSRIs))'), parent=antidepressants, body=_("Mood lifting drug."))

    yield productcat(name=_('Antidiarrheals'), parent=drug, body=_("Drugs used for the relief of diarrhea. Two main types of antidiarrheal preparations are simple adsorbent substances and drugs that slow down the contractions of the bowel muscles so that the contents are propelled more slowly."))
    yield productcat(name=_('Antiemetics'), parent=drug, body=_("Drugs used to treat nausea and vomiting."))
    yield productcat(name=_('Antifungals'), parent=drug, body=_("Drugs used to treat fungal infections, the most common of which affect the hair, skin, nails, or mucous membranes."))
    yield productcat(name=_('Antihistamines'), parent=drug, body=_("Drugs used primarily to counteract the effects of histamine, one of the chemicals involved in allergic reactions."))

    antihypertensives = productcat(name=_('Antihypertensives'), parent=drug, body=_("Drugs that lower blood pressure."))
    yield antihypertensives
    yield productcat(name=_('Diuretics (antihypertensives)'), parent=antihypertensives, body=_("Drugs that lower blood pressure."))
    yield productcat(name=_('Beta-blockers (antihypertensives)'), parent=antihypertensives, body=_("Drugs that lower blood pressure. Beta-adrenergic blocking agents, or beta-blockers for short, reduce the oxygen needs of the heart by reducing heartbeat rate."))
    yield productcat(name=_('Calcium channel blocker (antihypertensives)'), parent=antihypertensives, body=_("Drugs that lower blood pressure."))
    yield productcat(name=_('ACE (angiotensin- converting enzyme) inhibitors (antihypertensives)'), parent=antihypertensives, body=_("Drugs that lower blood pressure."))
    yield productcat(name=_('Sympatholytics (antihypertensives)'), parent=antihypertensives, body=_("Drugs that lower blood pressure."))

    yield productcat(name=_('Anti-Inflammatories'), parent=drug, body=_("Drugs used to reduce inflammation - the redness, heat, swelling, and increased blood flow found in infections and in many chronic noninfective diseases such as rheumatoid arthritis and gout."))
    yield productcat(name=_('Antineoplastics'), parent=drug, body=_("Drugs used to treat cancer."))
    yield productcat(name=_('Antipsychotics'), parent=drug, body=_("Drugs used to treat symptoms of severe psychiatric disorders. These drugs are sometimes called major tranquilizers."))
    yield productcat(name=_('Antipyretics'), parent=drug, body=_("Drugs that reduce fever."))
    yield productcat(name=_('Antivirals'), parent=drug, body=_("Drugs used to treat viral infections or to provide temporary protection against infections such as influenza."))

    sleeping_drug = productcat(name=_('Sleeping Drugs'), parent=drug, body=_("All such drugs have a sedative effect in low doses and are effective sleeping medications in higher doses. Comes in two varieties, Benzodiazepines & Barbiturates."))
    yield sleeping_drug
    yield productcat(name=_('Benzodiazepines (sleeping drug)'), parent=sleeping_drug, body=_("Benzodiazepines drugs are used more widely than barbiturates because they are safer, the side-effects are less marked, and there is less risk of eventual physical dependence."))
    yield productcat(name=_('Barbiturates (sleeping drug)'), parent=sleeping_drug, body=_("Sleeping drug."))

    yield productcat(name=_('Bronchodilators'), parent=drug, body=_("Drugs that open up the bronchial tubes within the lungs when the tubes have become narrowed by muscle spasm. Bronchodilators ease breathing in diseases such as asthma."))
    yield productcat(name=_('Cold Cures'), parent=drug, body=_("Although there is no drug that can cure a cold, the aches, pains, and fever that accompany a cold can be relieved by aspirin or acetaminophen often accompanied by a decongestant, antihistamine, and sometimes caffeine."))
    yield productcat(name=_('Corticosteroids'), parent=drug, body=_("These hormonal preparations are used primarily as anti-inflammatories in arthritis or asthma or as immunosuppressives, but they are also useful for treating some malignancies or compensating for a deficiency of natural hormones in disorders such as Addison's disease."))
    yield productcat(name=_('Cough Suppressants'), parent=drug, body=_("""Simple cough medicines, which contain substances such as honey, glycerine, or menthol, soothe throat irritation but do not actually suppress coughing. They are most soothing when taken as lozenges and dissolved in the mouth. As liquids they are probably swallowed too quickly to be effective. A few drugs are actually cough suppressants. There are two groups of cough suppressants: those that alter the consistency or production of phlegm such as mucolytics and expectorants; and those that suppress the coughing reflex such as codeine (narcotic cough suppressants), antihistamines, dextromethorphan and isoproterenol (non-narcotic cough suppressants)."""))
    yield productcat(name=_('Cytotoxics'), parent=drug, body=_("Drugs that kill or damage cells. Cytotoxics are used as antineoplastics (drugs used to treat cancer) and also as immunosuppressives."))
    yield productcat(name=_('Decongestants'), parent=drug, body=_("Drugs that reduce swelling of the mucous membranes that line the nose by constricting blood vessels, thus relieving nasal stuffiness."))
    yield productcat(name=_('Diuretics'), parent=drug, body=_("Drugs that increase the quantity of urine produced by the kidneys and passed out of the body, thus ridding the body of excess fluid. Diuretics reduce water logging of the tissues caused by fluid retention in disorders of the heart, kidneys, and liver. They are useful in treating mild cases of high blood pressure."))
    yield productcat(name=_('Expectorant'), parent=drug, body=_("A drug that stimulates the flow of saliva and promotes coughing to eliminate phlegm from the respiratory tract."))
    yield productcat(name=_('Hormones'), parent=drug, body=_("Chemicals produced naturally by the endocrine glands (thyroid, adrenal, ovary, testis, pancreas, parathyroid). In some disorders, for example, diabetes mellitus, in which too little of a particular hormone is produced, synthetic equivalents or natural hormone extracts are prescribed to restore the deficiency. Such treatment is known as hormone replacement therapy."))
    yield productcat(name=_('Hypoglycemics (Oral)'), parent=drug, body=_("Drugs that lower the level of glucose in the blood. Oral hypoglycemic drugs are used in diabetes mellitus if it cannot be controlled by diet alone, but does require treatment with injections of insulin."))
    yield productcat(name=_('Immunosuppressives'), parent=drug, body=_("Drugs that prevent or reduce the body's normal reaction to invasion by disease or by foreign tissues. Immunosuppressives are used to treat autoimmune diseases (in which the body's defenses work abnormally and attack its own tissues) and to help prevent rejection of organ transplants."))
    yield productcat(name=_('Laxatives'), parent=drug, body=_("""Drugs that increase the frequency and ease of bowel movements, either by stimulating the bowel wall (stimulant laxative), by increasing the bulk of bowel contents (bulk laxative), or by lubricating them (stool-softeners, or bowel movement-softeners). Laxatives may be taken by mouth or directly into the lower bowel as suppositories or enemas. If laxatives are taken regularly, the bowels may ultimately become unable to work properly without them."""))
    yield productcat(name=_('Muscle Relaxants'), parent=drug, body=_("Drugs that relieve muscle spasm in disorders such as backache. Antianxiety drugs (minor tranquilizers) that also have a muscle-relaxant action are used most commonly."))
    yield productcat(name=_('Sedatives'), parent=drug, body=_("Same as Antianxiety drugs."))
    yield productcat(name=_('Sex Hormones (Female)'), parent=drug, body=_("""There are two groups of these hormones (estrogens and progesterone), which are responsible for development of female secondary sexual characteristics. Small quantities are also produced in males. As drugs, female sex hormones are used to treat menstrual and menopausal disorders and are also used as oral contraceptives. Estrogens may be used to treat cancer of the breast or prostate, progestins (synthetic progesterone to treat endometriosis)."""))
    yield productcat(name=_('Sex Hormones (Male)'), parent=drug, body=_("""Androgenic hormones, of which the most powerful is testosterone, are responsible for development of male secondary sexual characteristics. Small quantities are also produced in females. As drugs, male sex hormones are given to compensate for hormonal deficiency in hypopituitarism or disorders of the testes. They may be used to treat breast cancer in women, but either synthetic derivatives called anabolic steroids, which have less marked side- effects, or specific anti-estrogens are often preferred. Anabolic steroids also have a "body building" effect that has led to their (usually nonsanctioned) use in competitive sports, for both men and women."""))
    yield productcat(name=_('Tranquilizer'), parent=drug, body=_("This is a term commonly used to describe any drug that has a calming or sedative effect. However, the drugs that are sometimes called minor tranquilizers should be called antianxiety drugs, and the drugs that are sometimes called major tranquilizers should be called antipsychotics."))
    yield productcat(name=_('Vitamins'), parent=drug, body=_("Chemicals essential in small quantities for good health. Some vitamins are not manufactured by the body, but adequate quantities are present in a normal diet. People whose diets are inadequate or who have digestive tract or liver disorders may need to take supplementary vitamins."))

    # source https://www.britannica.com/topic/list-of-medical-tests-and-diagnostic-procedures-2074273
    yield productcat(name=_("Cellular and chemical analysis"), parent=test)
    yield productcat(name=_("Diagnostic imaging"), parent=test)
    yield productcat(name=_("Genetic testing"), parent=test)
    yield productcat(name=_("Measurement"), parent=test)
    yield productcat(name=_("Physical and visual examination"), parent=test)
