# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from copy import deepcopy

from lino.utils.html import E, tostring, fromstring

from django.utils.translation import gettext
from django.contrib.contenttypes.fields import GenericRelation

from lino.api import _
from lino_xl.lib.cal.models import *

from lino_medico.lib.courses.mixins import VariableSlotRelatable
from lino_medico.lib.medico.user_types import UserTypes

class Guest(Guest):

    class Meta(Guest.Meta):
        abstract = dd.is_abstract_model(__name__, 'Guest')

    def before_state_change(self, ar, old, new):
        super().before_state_change(ar, old, new)


class UpdateEntriesByEvent(UpdateEntriesByEvent):
    sort_index = 10
    button_text = _("⚡ Update calendar")  # 26A1
    combo_group = "update"


class UpdateChildEntries(dd.Action):
    select_rows = True
    label = _("Update child entries")
    button_text = _("🗱 Update child entries") #U+1F5F1
    combo_group = "update"

    def run_from_ui(self, ar, **kw):
        obj = ar.selected_rows[0]
        data = dict()
        for fld in obj.sibling_related_fields:
            data[fld] = getattr(obj, fld)
        if not obj.child_entries.count():
            if not obj.has_child_entries:
                raise Exception(_("This entry does NOT have any child entries!"))
            obj.update_children(**data)
        else:
            obj.child_entries.filter(disjoint_from_sibling=False).update(**data)
        ar.success(refresh=True)

class BulkUpdateSiblingEntries(dd.Action):
    select_rows = True
    label = _("Update sibling entries")
    button_text = _("🗱 Update sibling entries") #U+1F5F1
    combo_group = "update"

    def run_from_ui(self, ar, **kw):
        obj = ar.selected_rows[0]
        if not obj.parent_entry.exists():
            raise Exception(_("This entry does not have any sibling entries!"))
        if obj.disjoint_from_sibling:
            raise Exception(_("Cannot do bulk update from a disjoint sibling"))
        data = dict()
        for fld in obj.sibling_related_fields:
            data[fld] = getattr(obj, fld)
        obj.parent_entry.first().child_entries.filter(
            disjoint_from_sibling=False
        ).update(**data)
        ar.success(refresh=True)

class JoinAllSiblingEntries(dd.Action):
    select_rows = True
    label = _("Join all sibling")
    button_text = "⨝"
    # U+2A1D

    def run_from_ui(self, ar, **kw):
        obj = ar.selected_rows[0]
        if not obj.parent_entry.exists():
            raise Exception(_("This entry does not have any sibling entries!"))
        seqs = obj.parent_entry.first().child_entries
        ref_sibling = obj if not obj.disjoint_from_sibling else seqs.filter(disjoint_from_sibling=False).first() if seqs.filter(disjoint_from_sibling=False).exists() else None
        if ref_sibling is None:
            raise Exception(_("Can not join siblings because all of the siblings are disjoint(s)!"))
        data = dict(disjoint_from_sibling=False)
        for fld in ref_sibling.sibling_related_fields:
            data[fld] = getattr(ref_sibling, fld)
        seqs.filter(disjoint_from_sibling=True).update(**data)
        ar.success(refresh=True)


class HierarchicalEntry(dd.Model):

    """

    .. currentmodule:: lino_xl.lib.cal

    .. class:: HierarchicalEntry
        :noindex:

        As the name :class:`HierarchicalEntry` suggests, there can exists some
        children of a calender :class:`Event` when the
        :class:`HierarchicalEntry` is inherited by some subclass of
        :class:`Event`. The parent relation of a child entry is stored in the
        :attr:`Event.owner` field. The default implementation of
        :attr:`Event.owner` field is that it contains a reference to a subclass
        of an :class:`EventGenerator`, which we override here. To mitigate the
        problems caused by the dependencies to an :class:`EventGenerator`,
        :class:`HierarchicalEntry` overrides the :meth:`Event.has_auto_events`
        method, which simply returns `False` turning off the functionalities of
        an :class:`EventGenerator`.

        :class:`HierarchicalEntry` adds a GenericRelation to :class:`Event` on
        the :attr:`Event.owner` field, so that the children of an :class:`Event`
        are accessible through :attr:`Event.child_entries` `related object
        manager` and likewise the parent of a child entry is accessible through
        the :attr:`Event.parent_entry` `related object manager`.

        .. attribute:: parent_entry

            Related object manager to look up the :attr:`Event.owner`
            (:term:`parent entry`).

        .. attribute:: child_entries

            Related object manager to look up the :term:`child entries`.

        .. attribute:: has_child_entries

            Whether the :class:`Event` has :term:`child entries`. If so, the
            methods :meth:`create_children <HierarchicalEntry.create_children>`
            and :meth:`update_children <HierarchicalEntry.update_children>` will
            be called in consequent cases.

        .. attribute:: child_event_type

            Instance of an :class:`EventType` to set as the
            :attr:`Event.event_type` while creating a children.

        .. attribute:: disjoint_from_sibling

            A boolean field to indicate whether the entry is disjoint from
            :term:`sibling entries` or not. Used to filter out customized
            entries while bulk updating :term:`sibling entries`.

        .. attribute:: sibling_related_fields

            A tuple of :class:`Event` fields which should be updated while bulk
            updating sibling entries.

        .. attribute:: update_child_entries

            An instance of :class:`UpdateChildEntries` action.

        .. attribute:: update_sibling_entries

            An instance of :class:`BulkUpdateSiblingEntries` action.

        .. attribute:: force_join_sibling

            An instance of :class:`JoinAllSiblingEntries` action.

        .. method:: create_children(self, state=None, duplicate=True, **kwargs)

            The method that gets called while creating the :term:`child entries`.

        .. method:: update_children(self, **kwargs)

            The method that gets called while updating the :term:`child_entries`.

    .. class:: UpdateChildEntries
        :noindex:

        Action which bulk updates the :term:`child entries` of an :class:`Event`
        by looking up the :attr:`Event.sibling_related_fields <HierarchicalEntry.sibling_related_fields>`
        taking the value from the :class:`Event` itself and putting them into
        the :term:`child entries`. Does **NOT** updates the :term:`child
        entries`  whos :attr:`disjoint_from_sibling <HierarchicalEntry.disjoint_from_sibling>`
        is set to `True`.

    .. class:: BulkUpdateSiblingEntries
        :noindex:

        Action which bulk updates the :term:`sibling entries` of an
        :class:`Event` by looking up the :attr:`Event.sibling_related_fields <HierarchicalEntry.sibling_related_fields>`
        taking the value from the :class:`Event` itself and putting them into
        the :term:`sibling entries`. Does **NOT** updates the :term:`sibling entries`
        whos :attr:`disjoint_from_sibling <HierarchicalEntry.disjoint_from_sibling>`
        is set to `True`.

    .. class:: JoinAllSiblingEntries
        :noindex:

        Action which takes the :term:`sibling entries` whos
        :attr:`disjoint_from_sibling <HierarchicalEntry.disjoint_from_sibling>`
        is set to `True` and bulk updates them by setting the
        :attr:`disjoint_from_sibling <HierarchicalEntry.disjoint_from_sibling>`
        to `False` and also updates the :attr:`sibling_related_fields <HierarchicalEntry.sibling_related_fields>`
        values which it takes from the first sibling entry that it finds whos
        attribute :attr:`disjoint_from_sibling <HierarchicalEntry.disjoint_from_sibling>`
        is `False`.

    Implementation detail
    =====================

    Must define (or override) the methods :meth:`create_children <HierarchicalEntry.create_children>`
    and :meth:`update_children <HierarchicalEntry.update_children>`.

    .. glossary::

        child entries

            All entries whos :attr:`Event.parent_entry <HierarchicalEntry.parent_entry>`
            is the same :class:`Event` in contrast to the :term:`parent entry` itself.

        parent entry

            Refers to the entry stored in the :attr:`Event.parent_entry <HierarchicalEntry.parent_entry>`

        sibling entries

            All the other entries whos :attr:`Event.parent_entry <HierarchicalEntry.parent_entry>`
            is the same :class:`Event` in contrast to entries themselves.

    """

    sibling_related_fields = (
        'summary',
        'user',
        'assigned_to',
        'description',
        'event_type',
        'notify_before',
        'notify_unit'
    )

    def has_auto_events(self):
        # In case this event is the owner (supposedly an EventGenerator but NOT)
        # of some other event
        # returns `False` to turn off the functionalities of an EventGenerator
        return False

    class Meta:
        abstract = True

    has_child_entries = dd.BooleanField(_("Create child event"), default=False)
    child_event_type = dd.ForeignKey('cal.EventType', on_delete=models.SET_NULL,
        related_name='entry_by_child_entry_type', null=True, blank=True)
    disjoint_from_sibling = dd.BooleanField(_("Disjoin from sibling"), default=False)

    update_child_entries = UpdateChildEntries()
    update_sibling_entries = BulkUpdateSiblingEntries()
    force_join_sibling = JoinAllSiblingEntries()

    child_entries = GenericRelation('self',
        content_type_field='owner_type',
        object_id_field='owner_id',
        related_query_name='parent_entry')

    def create_children(self, state=None, duplicate=True, **kwargs):
        raise NotImplementedError

    def do_before_create_children(self, **kw):
        if self.child_event_type is not None:
            kw.update(event_type=self.child_event_type)
        return kw

    def update_children(self, **kwargs):
        raise NotImplementedError

class VariableSlotRelatableEntry(HierarchicalEntry, VariableSlotRelatable):
    """

    .. currentmodule:: lino_xl.lib.cal

    .. class:: VariableSlotRelatableEntry
        :noindex:

        A subclass of :class:`HierarchicalEntry`, requires
        :class:`Slot <lino_xl.lib.courses.Slot>` as
        :class:`Controllable <lino.modlib.gfks.Controllable>`.

        The first use case is in :mod:`lino_medio`, where a
        :class:`Slot <lino_xl.lib.courses.Slot>` inherits from
        :class:`Controllable <lino.modlib.gfks.Controllable>`.

        Adds a field :attr:`average_dispatch_time`. If this field is None, while
        creating slots it will look into the owner field for
        `average_dispatch_time`.

        .. attribute:: average_dispatch_time

            A duration field which specifies the span of a
            :class:`Slot <lino_xl.lib.courses.Slot>`.

    """
    class Meta:
        abstract = True

    slots = GenericRelation('courses.Slot',
        content_type_field='owner_type',
        object_id_field='owner_id',
        related_query_name='cal_entry')

    def get_child_event_detail(self, slot, **kw):
        return kw

    def do_before_create_children(self, **kw):
        super().do_before_create_children()
        kw.update(state=self.state)
        return kw

    def get_slots(self):
        if self.average_dispatch_time is not None:
            # if not self.slots.exists():
            self.create_or_update_slots()
            return self.slots.all()
        else:
            owner = self.owner
            while not owner.slots.exists():
                owner = owner.owner
                if owner is None:
                    raise Exception(_("There seem to be associated time slots related to this event!"))
            return owner.slots.all()

    def before_state_change(self, ar, old, new):
        if new == EntryStates.cancelled:
            if self.child_entries.filter(state=EntryStates.took_place).exists():
                raise Exception(_("Can NOT cancel this entry. Because it has child entries that already has taken place!"))
        super().before_state_change(ar, old, new)

    def after_state_change(self, ar, old, new):
        super().after_state_change(ar, old, new)
        if self.has_child_entries:
            self.update_children()

    def update_children(self, **kw):
        self._update_children(**kw)

    def _update_children(self, **kw):
        if self.has_child_entries:
            slots = self.get_slots()
            if self.state not in [EntryStates.took_place, EntryStates.cancelled]:
                if not self.child_entries.count():
                    self.create_children(**kw)
                else:
                    s = slots.first()
                    e = self.child_entries.get(sequence=s.seqno)
                    tcc = self.child_entries.count()
                    umcc = self.child_entries.exclude(
                        state=EntryStates.took_place
                    ).exclude(
                        state=EntryStates.cancelled
                    ).count()
                    if tcc != umcc:
                        raise Exception(f"Operation NOT allowed: you have ({tcc - umcc}) modified children. Try 'force join sibling'.")
                    if s.start_time != e.start_time or s.end_time != e.end_time:
                        self.child_entries.all().delete()
                        self.create_children(**kw)
                    elif self.state != e.state:
                        self.child_entries.all().update(state=self.state)
            elif self.state == EntryStates.took_place:
                self.child_entries.filter(
                    state=EntryStates.published
                ).update(state=EntryStates.cancelled)
            elif self.state == EntryStates.cancelled:
                self.child_entries.all().delete()
            else:
                self.child_entries.all().update(state=self.state)
        else:
            self.child_entries.all().delete()

    def create_children(self, state=None, duplicate=True, **kwargs):
        kwargs = self.do_before_create_children(**kwargs)
        event = None
        if duplicate:
            event = deepcopy(self)
            event.id = None
            event.owner = self
        for slot in self.get_slots():
            kwargs.update(**slot.get_event_kwargs())
            kwargs = self.get_child_event_detail(slot, **kwargs)
            if event is not None:
                e = deepcopy(event)
            else:
                e = self.__class__() # NOT tested! Take caution when giving duplicate as `False`
            for k, v in kwargs.items():
                setattr(e, k, v)
            e.full_clean()
            e.save()


class Event(Event, VariableSlotRelatableEntry):

    quick_search_fields = """summary description start_date
    user__partner__person__worker__first_name
    user__partner__person__worker__last_name
    user__partner__person__worker__middle_name
    user__partner__person__worker__specialties_by_physician__specialty__designation
    user__association__country__isocode user__association__country__short_code
    user__association__country__iso3 user__association__country__name
    user__association__city__name"""

    class Meta(Event.Meta):
        abstract = dd.is_abstract_model(__name__, 'Event')

    update_events = UpdateEntriesByEvent()

    def do_before_create_children(self, **kw):
        kw = super().do_before_create_children(**kw)
        if self.line.exists():
            kw.update(summary='appointment')
        return kw

    def variable_slots(self):
        if self.line.exists() and self.line.first().event_type.ref == 'doctors_consultation':
            return True
        return super().variable_slots()

    @dd.displayfield(_("Weekday"))
    def weekday(self, ar):
        if self.end_date is None:
            date = self.start_date
        elif self.start_date <= dd.today() <= self.end_date:
            date = dd.today()
        else:
            date = self.start_date
        return Weekdays.choices[date.weekday()][0].text

    def do_before_create_children(self, **kw):
        kw = super().do_before_create_children(**kw)
        kw.update(summary='appointment')
        kw.update(event_type=rt.models.cal.EventType.objects.get(ref='appointment'))
        return kw

    def as_search_item(self, ar):
        calling_user = ar.get_user()
        elems = []
        if calling_user.user_type == UserTypes.user:
            s = self.slots.filter(enrolment__isnull=False, enrolment__pupil=calling_user.person)
            if s.exists():
                elems.append(E.p(gettext("You have requested for the following slot: ") + s[0].description))

        return tostring(E.div(
            E.p(
                E.strong(
                    self.user.worker.full_name
                ),
                gettext(" (is available) at "),
                E.strong(
                    self.user.association.full_name
                )
            ),
            E.p(
                E.strong(gettext("On: ")),
                self.start_date.isoformat(),
                gettext(" from "),
                self.start_time.isoformat(),
                gettext(" to "),
                self.end_time.isoformat()
            ),
            E.p(
                E.strong(gettext("Address: ")),
                tostring(self.user.association.address_column)
            ),
            *elems
        ))

from .ui import *
