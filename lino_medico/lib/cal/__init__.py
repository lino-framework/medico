# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""This is Lino Medico's calendar module. See :doc:`/specs/cal`.

.. autosummary::
   :toctree:

    utils

"""

from lino.api import _
from lino_xl.lib.cal import *


class Plugin(Plugin):
    verbose_name = _("Calendar")

    extends_models = ['Event', 'Guest']

    def setup_main_menu(self, site, user_type, m, ar=None):
        m = m.add_menu(self.app_label, self.verbose_name)

        m.add_action('cal.AppointmentSchedules')  # string spec to allow overriding
        m.add_action('cal.OverdueAppointments')
        m.add_action('cal.MyUnconfirmedAppointments')

        # m.add_separator('-')
        # m  = m.add_menu("tasks",_("Tasks"))
        # m.add_action('cal.MyTasks')
        # m.add_action(MyTasksToDo)
        m.add_action('cal.MyGuests')
        m.add_action('cal.MyPatronsGuests')
        # m.add_action('cal.MyPresences')
        m.add_action('cal.MyOverdueAppointments')
        # m.add_action('cal.DailyPlanner')

    def setup_config_menu(self, site, user_type, m, ar=None):
        m = m.add_menu(self.app_label, self.verbose_name)
        m.add_action('cal.Calendars')
        # m.add_action('cal.MySubscriptions')
        m.add_action('cal.AllRooms')
        # m.add_action('cal.Priorities')
        m.add_action('cal.RecurrentEvents')
        # m.add_action(AccessClasses)
        # m.add_action(EventStatuses)
        # m.add_action(TaskStatuses)
        # m.add_action(EventTypes)
        m.add_action('cal.GuestRoles')
        # m.add_action(GuestStatuses)
        m.add_action('cal.EventTypes')
        m.add_action('cal.EventPolicies')
        m.add_action('cal.RemoteCalendars')

    def setup_explorer_menu(self, site, user_type, m, ar=None):
        m = m.add_menu(self.app_label, self.verbose_name)
        # m.add_action('cal.Days')
        m.add_action('cal.AllEntries')
        m.add_action('cal.Tasks')
        m.add_action('cal.AllGuests')
        m.add_action('cal.Subscriptions')
        # m.add_action(Memberships)
        m.add_action('cal.EntryStates')
        m.add_action('cal.GuestStates')
        m.add_action('cal.TaskStates')
        m.add_action('cal.PlannerColumns')
        # m.add_action('cal.AccessClasses')
        m.add_action('cal.DisplayColors')
        # m.add_action(RecurrenceSets)

    def get_dashboard_items(self, user):
        from lino.core.dashboard import ActorItem

        if user.is_authenticated:
            # yield self.site.models.cal.LastWeek
            # yield self.site.models.cal.ComingWeek
            yield self.site.models.cal.MyTasks
            yield ActorItem(
                self.site.models.cal.MyEntries, min_count=None)
            yield self.site.models.cal.MyOverdueAppointments
            yield self.site.models.cal.MyUnconfirmedAppointments
            yield self.site.models.cal.MyPresences
        else:
            yield self.site.models.cal.PublicEntries
