# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import rt, dd, _

ET = rt.models.cal.EventType

def objects():
    yield ET(ref='availability', name=_("Availability"), is_appointment=False)
    yield ET(ref='appointment', name=_("Appointment"), is_appointment=True, locks_user=True)
    yield ET(ref='doctors_consultation', name=_("Doctors consultation"), is_appointment=True, locks_user=True, default_duration=dd.plugins.courses.physicians_average_dispatch_time)
    yield ET(ref='group_session', name=_("Group session"), is_appointment=True, locks_user=True)
