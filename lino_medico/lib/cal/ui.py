# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from django.db import models

from lino.utils.html import tostring

from lino.api import rt, _
from lino_xl.lib.cal.ui import *
from lino.core.actions import ShowDetail, ShowTable

from lino_medico.lib.medico.user_types import Patient, ReceptionClerk, Physician

# class MyEntries(MyEntries):
#
#     @classmethod
#     def get_request_queryset(cls, ar, **filter):
#         calling_user = ar.get_user()
#         return super().get_request_queryset(ar, **filter).filter(user=calling_user)


class EventDetail(EventDetail):

    general_base = dd.Panel("""
    event_type summary notify
    weekday
    start end #all_day #duration #state
    room project owner workflow_buttons
    # owner created:20 modified:20
    description #outbox.MailsByController
    """, label=_("General"))

class AppointmentScheduleDetail(EventDetail):
    general_base = dd.Panel("""
    event_type summary notify
    weekday
    start end #all_day #duration #state
    room project owner workflow_buttons
    # owner created:20 modified:20
    description #outbox.MailsByController
    cal.AppointmentBySchedule
    """, label=_("General"))

class MyUnconfirmedAppointments(MyUnconfirmedAppointments):

    @classmethod
    def param_defaults(self, ar, **kw):
        kw = super(MyUnconfirmedAppointments, self).param_defaults(ar, **kw)
        # kw.update(end_date=None)
        return kw

    # @classmethod
    # def get_request_queryset(cls, ar, **filter):
    #     return super().get_request_queryset(ar, **filter).filter(user=ar.get_user())

class MyGuests(MyGuests):
    column_names = "booking workflow_buttons remark *"

    @dd.displayfield(_("Booking"))
    def booking(cls, obj, ar):
        if obj is not None:
            slot = obj.event.slots.annotate(match_found=models.Case(
                models.When(models.Exists(rt.models.courses.Enrolment.objects.filter(
                    slot__pk=models.OuterRef('pk'),
                    pupil=obj.partner
                )), then=models.Value(True)),
                default=models.Value(False),
                output_field=models.BooleanField()
            )).filter(match_found=True)[0]
            return str(obj.event.start_date) + " " + slot.description

class MyPatronsGuests(MyGuests):
    required_roles = dd.login_required((ReceptionClerk,))

    column_names = "booking event__line__contact_person partner workflow_buttons remark *"

    # params_layout = MyGuests.params_layout + " associate_contact_person"

    @classmethod
    def get_request_queryset(cls, ar, **filter):
        # return super().get_request_queryset(ar, **filter)
        calling_user = ar.get_user()
        return cls.model.objects.filter(
            event__line__associate_contact_person=calling_user.partner,
            event__line__company=calling_user.association
        )

    @classmethod
    def param_defaults(cls, ar, **kw):
        kw = super().param_defaults(ar, **kw)
        kw.update(user=None)
        return kw

    @classmethod
    def get_row_permission(cls, obj, ar, state, ba):
        return cls.get_request_queryset(ar).filter(pk=obj.pk).exists()

class AppointmentSchedules(MyEntries):
    label = _("My availability")

    detail_layout = AppointmentScheduleDetail()

    @classmethod
    def param_defaults(cls, ar, **kw):
        kw = super().param_defaults(ar, **kw)
        kw.update(show_appointments=dd.YesNo.no)
        return kw

class AppointmentBySchedule(MyEntries):
    master_key = 'parent_entry'
    required_roles = dd.login_required((Physician, ReceptionClerk))
    allow_create = False

    # column_names = "description enrolment enrolment__pupil case enrolment__state"

    # detail_layout = """
    # name start_time end_time
    # # enrolment__pupil
    # # courses.EnrolmentsBySlot
    # """

class AppointmentByScheduleEnrollable(MyEntries):
    master_key = 'parent_entry'
    label = _("Appointment slots")
    required_roles = dd.login_required(Patient)
    detail_layout = None

    @classmethod
    def param_defaults(cls, ar, **kw):
        kw = super().param_defaults(ar, **kw)
        kw.update(user=None)
        return kw

    @classmethod
    def get_row_permission(cls, obj, ar, state, ba):
        perm = super().get_row_permission(obj, ar, state, ba)
        if perm == False:
            if (
                state == rt.models.cal.EntryStates.published
                and 'enrole' in ar.rqdata
            ):
                perm = True
        return perm

    column_names = "description enrole"

    @dd.displayfield(_("Slot"))
    def description(self, obj, ar):
        return str(obj.sequence) + " (" + str(obj.start_time) + "-" + str(obj.end_time) + ")"

    @dd.virtualfield(models.BooleanField(_("Enrole")), editable=True)
    def enrole(self, obj, ar):
        calling_user = ar.get_user()
        if 'enrole' in ar.rqdata:
            print('='*80)
            print(obj, ar)
        guest = rt.models.cal.Guest.objects.filter(
            partner=calling_user.partner, event=obj).exists()
        enroled = rt.models.courses.Enrolment.objects.filter(
            cal_entry=obj, pupil=calling_user.partner).exists()
        return guest and enroled

    # @classmethod
    # def get_request_queryset(cls, ar, **filter):
    #     calling_user = ar.get_user()
    #     query = models.Q(enrolment__isnull=True)
    #     if calling_user.user_type == UserTypes.user:
    #         query = query | models.Q(enrolment__pupil=calling_user.person)
    #     return super().get_request_queryset(ar, **filter).filter(query).order_by('seqno')


class OneAppointmentSchedule(OneEvent):
    label = _("Appointment schedules")
    required_roles = dd.login_required((Patient,))

    detail_layout = """
    overview medico.SpecialtiesByPhysicianToEvent
    # courses.SlotsByCalEntryEnrolable
    """

    @dd.displayfield(_("Physician"))
    def name(cls, obj, ar):
        if obj is not None:
            return obj.user.worker.full_name

    @dd.displayfield(_("Overview"))
    def overview(cls, obj, ar):
        if obj is not None:
            return obj.as_search_item(ar)

    @classmethod
    def get_card_title(cls, ar, elem):
        return ar.obj2html(elem)

    @classmethod
    def get_row_permission(cls, obj, ar, state, ba):
        if not isinstance(ba.action, (ShowTable, ShowDetail)):
            return False
        if state is None:
            state = cls.get_row_state(obj)
        if state == rt.models.cal.EntryStates.published:
            return True
        return False
