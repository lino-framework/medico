# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

import datetime

from django.db import models

from lino.api import rt, dd, _
from lino.core import constants
from lino.modlib.search.roles import SiteSearcher

EntryStates = rt.models.cal.EntryStates

class SearchText(dd.Model):
    class Meta:
        app_label = 'search'
        abstract = dd.is_abstract_model(__name__, 'SearchText')

    search_text = dd.CharField(max_length=300)

# class SearchHistoryManager(models.Manager):
#
#     def get_requested_queryset(self, *arg, **kwargs):
#         return super().get_requested_queryset(*args, **kwargs).annotate(
#             rank=models.F('weight_index') + models.F('times_choosen')
#         )

class SearchHistory(dd.Model):

    # objects = SearchHistoryManager()

    class Meta:
        app_label = 'search'
        abstract = dd.is_abstract_model(__name__, 'SearchHistory')
        # ordering = ('rank', )

    search_text = dd.ForeignKey('search.SearchText')
    choosen_result = dd.RichTextField()
    detail_link = models.URLField()
    times_choosen = models.PositiveIntegerField(default=0)
    weight_index = models.PositiveIntegerField(default=0)

    def full_clean(self, *args, **kwargs):
        super().full_clean(*args, **kwargs)
        self.times_choosen += 1

class SearchDate:

    lookup_anyway = ['next']

    @classmethod
    def find_next_dow(day_index):
        day = dd.today()
        while day.weekday() != day_index:
            day += datetime.timedelta(days=1)
        return day

    @classmethod
    def day_keyword_map(cls):
        _keyword_map = {
            'today': dd.today(),
            'tomorrow': dd.today() + datetime.timedelta(days=1),
            'next': {
                'monday': cls.find_next_dow(0),
                'tuesday': cls.find_next_dow(1),
                'wednesday': cls.find_next_dow(2),
                'thursday': cls.find_next_dow(3),
                'friday': cls.find_next_dow(4),
                'saturday': cls.find_next_dow(5),
                'sunday': cls.find_next_dow(6),
            }
        }
        return _keyword_map

    @classmethod
    def keyword_map(cls):
        return cls.day_keyword_map()

    @classmethod
    def find_date_from_token(cls, tokens):
        assert type(tokens) == list or type(tokens) == tuple or type(tokens) == set
        tokens = {token.lower() for token in tokens}
        key_map = cls.day_keyword_map()
        def do_recurse(key_map):
            for token in tokens:
                for key, value in key_map.items():
                    if token in key or key in cls.lookup_anyway:
                        if key in cls.lookup_anyway:
                            assert type(value) == dict
                        if type(value) == dict:
                            return do_recurse(value)
                            break
                        else:
                            return value
        return do_recurse(key_map)

    @classmethod
    def find_value_from_token(cls, tokens):
        return cls.find_date_from_token(tokens)
