# -*- coding: UTF-8 -*-
# Copyright 2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""
.. autosummary::
   :toctree:

    models
    ui


"""

from lino.api import ad, _


class Plugin(ad.Plugin):
    "See :class:`lino.core.plugin.Plugin`."
    verbose_name = _("Local Exchange")

    def setup_main_menu(self, site, profile, m, ar=None):
        m = m.add_menu(self.app_label, self.verbose_name)
        m.add_action('lets.Offers')
        m.add_action('lets.Demands')

    def get_dashboard_items(self, user):
        yield self.site.models.lets.ActiveProducts
