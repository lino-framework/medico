# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

import datetime as dt

from lino.api import rt, _, dd
from lino_xl.lib.cal.models import Recurrences, EntryStates

def objects():
    Line = rt.models.courses.Line
    l = Line(
        name=_("Appointment schedule"),
        company=rt.models.contacts.Company.objects.get(name='Laksham General Hospital'),
        contact_role=rt.models.contacts.RoleType.objects.get(name='Physician'),
        associate_contact_role=rt.models.contacts.RoleType.objects.get(name='Reception clerk'),
        guest_role=rt.models.cal.GuestRole.objects.get(ref='patient'),
        event_type=rt.models.cal.EventType.objects.get(ref='doctors_consultation'),
        contact_person=rt.models.contacts.Worker.objects.get(first_name='Nazir'),
        associate_contact_person=rt.models.contacts.Worker.objects.get(first_name='Faruk'),
        tuesday=True,
        saturday=True,
        every=1,
        every_unit=Recurrences.weekly,
        room=rt.models.cal.Room.objects.get(name='101'),
        average_dispatch_time="0:10",
        start_time=dt.time(14),
        end_time=dt.time(20)
    )
    ar = l.get_default_table().request(rqdata={
        'case_enrolment_fee': 800,
        'case_continued_fee_vf': 600
    })
    l.before_ui_save(ar, None)
    yield l
    l.after_ui_save(ar, None)

    Event = rt.models.cal.Event
    ar = None
    for e in Event.objects.all()[:3]:
        if ar is None:
            ar = e.get_default_table().request()
        e.set_workflow_state(ar, ar.actor.workflow_state_field, EntryStates.published)

    # s = Event.objects.filter(state=EntryStates.published).first().slots.first()
    # s.after_ui_save(s.get_default_table().request(
    #     user=rt.models.users.User.objects.get(username='carlos'),
    #     rqdata={
    #         'enrole': 'true'
    #     }
    # ), None)
