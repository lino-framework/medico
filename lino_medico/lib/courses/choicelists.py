# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import dd, _
from lino_xl.lib.courses.choicelists import *

# ActivityLayouts.clear()

add = ActivityLayouts.add_item

add('gc', _("General consultation"), 'general_consultation')
add('uc', _("Urgent consultation"), 'urgent_consultation')

class Frequencies(dd.ChoiceList):
    verbose_name = _("Administration frequency unit")

add = Frequencies.add_item
add('H', _("Hourly"), 'hourly')
add('D', _("Daily"), 'daily')
add('W', _("Weekly"), 'weekly')
add('M', _("Monthly"), 'monthly')
