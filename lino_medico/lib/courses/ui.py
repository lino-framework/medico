# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from django.db import models

from lino.api import rt, dd, _
from lino_xl.lib.courses.ui import *
# from lino_xl.lib.courses.ui import (
#     Activities,
#     MyActivities,
#     AllActivities,
#     MyCoursesGiven,
#     DraftActivities,
#     ActiveActivities,
#     InactiveActivities,
#     ClosedActivities,
#     ActivitiesByTeacher,
#     ActivitiesByLine,
#     ActivitiesByLayout,
#     ActivitiesByTopic,
#     Lines,
#     Slots
# )

from lino_medico.lib.medico.user_types import Physician, SiteStaff, SiteAdmin, ReceptionClerk, Patient, UserTypes

class Activities(Activities):
    detail_layout = """
    name user line state
    """

    insert_layout = """
    name user line state
    """

    column_names = "name user line state"
    params_layout = "user line state"
    order_by = None

class MyActivities(MyActivities):
    detail_layout = """
    name user line state
    """

    column_names = "name user line state"
    params_layout = "name user line state"
    insert_layout = """
    name user line state
    """
    order_by = None


    @classmethod
    def param_defaults(cls, ar, **kw):
        kw = super().param_defaults(ar, *kw)
        if 'user' in kw:
            kw.pop('user')
        return kw

class AllActivities(AllActivities):
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    params_layout = "name user line state"
    insert_layout = """
    name user line state
    """
    order_by = None


class MyCoursesGiven(MyCoursesGiven):
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    insert_layout = """
    name user line state
    """
    params_layout = "name user line state"
    order_by = None


class DraftActivities(DraftActivities):
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    params_layout = "name user line state"
    insert_layout = """
    name user line state
    """
    order_by = None

class ActiveActivities(ActiveActivities):
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    params_layout = "name user line state"
    insert_layout = """
    name user line state
    """
    order_by = None

class InactiveActivities(InactiveActivities):
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    params_layout = "name user line state"
    insert_layout = """
    name user line state
    """
    order_by = None

class ClosedActivities(ClosedActivities):
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    params_layout = "name user line state"
    insert_layout = """
    name user line state
    """
    order_by = None

class ActivitiesByTeacher(ActivitiesByTeacher):
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    insert_layout = """
    name user line state
    """
    params_layout = "name user line state"
    order_by = None

class ActivitiesByLine(ActivitiesByLine):
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    params_layout = "name user line state"
    insert_layout = """
    name user line state
    """
    order_by = None

class ActivitiesByLayout(ActivitiesByLayout):
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    insert_layout = """
    name user line state
    """
    params_layout = "name user line state"
    order_by = None

class ActivitiesBySlot(ActivitiesBySlot):
    abstract = True
    detail_layout = """
    name user line state
    """
    column_names = "name user line state"
    params_layout = "name user line state"
    insert_layout = """
    name user line state
    """
    order_by = None

ActivitiesByTopic.insert_layout = """
name user line state
"""

class CourseDetail(dd.DetailLayout):
    main = """
    user line teacher pupil
    """

class EnrolmentDetail(dd.DetailLayout):
    main = """
    course
    """


class LineDetail(dd.DetailLayout):
    main = """
    id name ref
    contact_person associate_contact_person
    room case_enrolment_fee case_continued_fee_vf
    event_type guest_role every_unit every
    start_date start_time end_date end_time
    monday tuesday wednesday thursday friday saturday sunday
    average_dispatch_time
    description
    """

class LineInsert(dd.InsertLayout):
    main = """
    contact_person associate_contact_person
    room case_enrolment_fee case_continued_fee_vf
    every every_unit
    start_date start_time end_date end_time
    monday tuesday wednesday thursday friday saturday sunday
    average_dispatch_time
    """

class Lines(Lines):
    detail_layout = LineDetail()
    insert_layout = LineInsert()

    @classmethod
    def create_instance(cls, ar, **kw):
        kw.update(
            name=_("Appointment schedule"),
            company=ar.get_user().association,
            contact_role=rt.models.contacts.RoleType.objects.get(name='Physician'),
            associate_contact_role=rt.models.contacts.RoleType.objects.get(name='Reception clerk'),
            guest_role=rt.models.cal.GuestRole.objects.get(ref='patient'),
            event_type=rt.models.cal.EventType.objects.get(ref='doctors_consultation')
        )
        return super().create_instance(ar, **kw)

class PhysiciansOutdoorLines(Lines):
    label = _("Physician's schedules")

    required_roles = dd.login_required(SiteStaff)

class MyOutdoorLines(Lines):
    label = _("My schedules")
    required_roles = dd.login_required(Physician)

    @classmethod
    def get_request_queryset(cls, ar, **filter):
        calling_user = ar.get_user()
        return super().get_request_queryset(ar, **filter).filter(
            contact_person=calling_user.worker,
            company=calling_user.association
        )

class Slots(Slots):
    detail_layout = """
    name start_time end_time
    """

class Enrolments(dd.Table):
    label = _("Enrolment")
    model = 'courses.Enrolment'
    required_roles = dd.login_required((Physician, ReceptionClerk))
    column_names = 'user course pupil state'


class PrescriptionDetail(dd.DetailLayout):
    main = """
    course
    courses.SymptomsByPrescription
    diagnosis
    prognosis
    analysis
    recommendation
    courses.MedicinesByPrescription
    remarks
    """


class Prescriptions(dd.Table):
    label = _("Prescription")
    model = 'courses.Prescription'
    required_roles = dd.login_required((Physician,))

    detail_layout = PrescriptionDetail()

class SymptomsPerPrescription(dd.Table):
    label = _("Symptoms")
    model = 'courses.SymptomPerPrescription'
    required_roles = dd.login_required((Physician,))
    column_names = "prescription symptom"

class SymptomsByPrescription(SymptomsPerPrescription):
    master_key = 'prescription'
    column_names = "symptom"

class MedicinesPerPrescription(dd.Table):
    label = _("Medicines")
    model = 'courses.MedicinePerPrescription'
    required_roles = dd.login_required((Physician,))
    column_names = "prescription medicine"

class MedicinesByPrescription(MedicinesPerPrescription):
    master_key = 'prescription'
    column_names = "medicine medicine__unit medicine__value ad_freq ad_freq_unit"
