# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.translation import gettext

from lino.api import rt, dd, _
from lino.modlib.gfks.mixins import Controllable
from lino.modlib.system.mixins import RecurrenceSet
from lino.mixins import Created, Sequenced

from lino_xl.lib.courses.models import *
from lino_xl.lib.cal.mixins import EventGenerator
from lino_xl.lib.invoicing.mixins import InvoiceGenerator
from lino_xl.lib.accounting.mixins import Payable

from lino_medico.lib.medico.user_types import UserTypes

from .mixins import SlotRelatable, EntryRelatableSlot
from .choicelists import *


class Course(Duplicable, Printable):

    class Meta:
        app_label = 'courses'
        abstract = dd.is_abstract_model(__name__, 'Course')
        verbose_name = _("Case")
        verbose_name_plural = _('Cases')

    slot = dd.DummyField() # So that the ActivitiesBySlot does NOT break

    user = dd.ForeignKey('users.User')
    line = dd.ForeignKey('courses.Line')

    teacher = dd.ForeignKey(
        teacher_model,
        related_name='courses_by_teacher',
        verbose_name=teacher_label,
        blank=True, null=True)

    pupil = dd.ForeignKey(
        pupil_model,
        related_name='courses_by_pupil',
        verbose_name=_("Patient"),
        blank=True, null=True)

    description = dd.BabelTextField(_("Description"), blank=True)
    remark = models.TextField(_("Remark"), blank=True)

    quick_search_fields = 'name line__name line__topic__name'

    state = CourseStates.field(default='draft')

    max_places = models.PositiveIntegerField(
        pgettext("in a course", "Available places"),
        help_text=("Maximum number of participants"),
        blank=True, null=True)

    name = models.CharField(_("Designation"), max_length=100, blank=True)
    enrolments_until = models.DateField(
        _("Enrolments until"), blank=True, null=True)

    print_presence_sheet = PrintPresenceSheet(show_in_toolbar=False)
    print_presence_sheet_html = PrintPresenceSheet(
        show_in_toolbar=False,
        build_method='weasy2html',
        label=format_lazy(u"{}{}",_("Presence sheet"), _(" (HTML)")))


    @dd.displayfield(_("When"))
    def weekdays_text(self, ar):
        return ""

    @dd.displayfield(_("When"))
    def times_text(self, ar):
        return ""

    @dd.displayfield(_("Print"))
    def print_actions(self, ar):
        if ar is None:
            return ''
        elems = []
        elems.append(ar.instance_action_button(
            self.print_presence_sheet))
        elems.append(ar.instance_action_button(
            self.print_presence_sheet_html))
        return E.p(*join_elems(elems, sep=", "))

    def on_duplicate(self, ar, master):
        self.state = CourseStates.draft
        super(Course, self).on_duplicate(ar, master)


    @classmethod
    def add_param_filter(
            cls, qs, lookup_prefix='', show_exposed=None, **kwargs):
        qs = super(Course, cls).add_param_filter(qs, **kwargs)
        exposed_states = CourseStates.filter(is_exposed=True)
        fkw = dict()
        fkw[lookup_prefix + 'state__in'] = exposed_states
        if show_exposed == dd.YesNo.no:
            qs = qs.exclude(**fkw)
        elif show_exposed == dd.YesNo.yes:
            qs = qs.filter(**fkw)
        return qs

    @classmethod
    def get_registrable_fields(cls, site):
        for f in super(Course, cls).get_registrable_fields(site):
            yield f
        yield 'line'
        yield 'teacher'
        yield 'name'
        yield 'enrolments_until'

    def __str__(self):
        if self.name:
            return self.name
        if self.line_id is None:
            line = self._meta.verbose_name
        else:
            line = self.line
        return str(line)

    def get_detail_action(self, ar):
        if ar is not None and self.line_id:
            area = self.line.course_area
            if area:
                table = rt.models.resolve(area.courses_table)
                return table.get_request_detail_action(ar)
        return super(Course, self).get_detail_action(ar)


    def full_clean(self, *args, **kw):
        if self.line_id is not None:
            if self.id is None:
                descs = dd.field2kw(self.line, 'description')
                descs = dd.babelkw('description', **descs)
                for k, v in descs.items():
                    setattr(self, k, v)
            if self.every_unit is None:
                self.every_unit = self.line.every_unit
            if self.every is None:
                self.every = self.line.every
        super(Course, self).full_clean(*args, **kw)

    def get_overview_elems(self, ar):
        elems = []
        # elems.append(ar.obj2html(self))
        elems.append(self.as_summary_item(ar))
        if self.teacher_id:
            elems.append(" / ")
            # elems.append(ar.obj2html(self.teacher))
            elems.append(self.teacher.as_summary_item(ar))
        return elems

    @dd.displayfield(_("Calendar entries"))
    def events_text(self, ar=None):
        if cal is not None:
            return ', '.join([
                day_and_month(e.start_date)
                for e in self.events_by_course().order_by('start_date')])

    def events_by_course(self, **kwargs):
        ct = rt.models.contenttypes.ContentType.objects.get_for_model(
            self.__class__)
        kwargs.update(owner_type=ct, owner_id=self.id)
        return rt.models.cal.Event.objects.filter(**kwargs)

    def get_places_sum(self, today=None, **flt):
        Enrolment = rt.models.courses.Enrolment
        PeriodEvents = rt.models.system.PeriodEvents
        qs = Enrolment.objects.filter(course=self, **flt)
        # see voga.projects.voga2.tests.test_max_places
        if today is None:
            rng = DateRangeValue(dd.today(), None)
            qs = PeriodEvents.active.add_filter(qs, rng)
        else:
            qs = PeriodEvents.active.add_filter(qs, today)
        # logger.info("20160502 %s", qs.query)
        res = qs.aggregate(models.Sum('places'))
        # logger.info("20140819 %s", res)
        return res['places__sum'] or 0

    def get_free_places(self, today=None):
        if not self.max_places:
            return None  # _("Unlimited")
        return self.max_places - self.get_used_places(today)

    def get_used_places(self, today=None):
        states = EnrolmentStates.filter(uses_a_place=True)
        return self.get_places_sum(today, state__in=states)

    # @dd.displayfield(_("Free places"), max_length=5)
    @dd.virtualfield(models.IntegerField(_("Free places")))
    def free_places(self, ar=None):
        # if not self.max_places:
        #     return None  # _("Unlimited")
        return self.get_free_places()

    @dd.virtualfield(models.IntegerField(_("Requested")))
    def requested(self, ar):
        return self.get_places_sum(state=EnrolmentStates.requested)
        # pv = dict(start_date=dd.today())
        # pv.update(state=EnrolmentStates.requested)
        # return rt.models.courses.EnrolmentsByCourse.request(
        #     self, param_values=pv)

    @dd.virtualfield(models.IntegerField(_("Confirmed")))
    def confirmed(self, ar):
        return self.get_places_sum(state=EnrolmentStates.confirmed)
        # pv = dict(start_date=dd.today())
        # pv.update(state=EnrolmentStates.confirmed)
        # return rt.models.courses.EnrolmentsByCourse.request(
        #     self, param_values=pv)

    @dd.virtualfield(models.IntegerField(_("Trying")))
    def trying(self, ar):
        return self.get_places_sum(state=EnrolmentStates.trying)

    @dd.requestfield(_("Enrolments"))
    def enrolments(self, ar):
        return self.get_enrolments(start_date=dd.today())

    def get_enrolments(self, **pv):
        # pv = dict(start_date=sd, end_date=dd.today())
        return rt.models.courses.EnrolmentsByCourse.request(
            self, param_values=pv)

class Enrolment(Enrolment, Sequenced, Payable, InvoiceGenerator):

    class Meta(Enrolment.Meta):
        verbose_name = _("Enrolment")
        verbose_name_plural = _("Enrolments")
        abstract = dd.is_abstract_model(__name__, 'Enrolment')
        unique_together = None

    cal_entry = dd.ForeignKey('cal.Event')

    def get_invoiceable_partner(self):
        return self.pupil

    def get_invoiceable_payment_term(self):
        return rt.models.accounting.PaymentTerm.objects.get(ref='PIA')

    def get_invoiceable_events(self, start_date, max_date):
        pass
        # return


dd.update_field(Enrolment, 'pupil', verbose_name=_("Patient"))

class Slot(Slot, Controllable, EntryRelatableSlot):

    class Meta(Slot.Meta):
        abstract = dd.is_abstract_model(__name__, 'Slot')

    # @dd.virtualfield(dd.ForeignKey('courses.Course'), editable=True)
    # def case(self, ar):
    #     try:
    #         return self.enrolment.course
    #     except:
    #         pass
    #
    # @dd.chooser()
    # def case_choices(cls, cal_entry, ar):
    #     assert ar.get_user().user_type == UserTypes.user
    #     return rt.models.courses.Course.objects.filter(
    #         pupil__pk=ar.get_user().partner.pk,
    #         teacher=cal_entry.user.worker
    #     )
    #
    # def after_ui_save(self, ar, cw):
    #     super().after_ui_save(ar, cw)
    #     calling_user = ar.get_user()
    #     if calling_user.user_type == UserTypes.user:
    #         Enrolment = rt.models.courses.Enrolment
    #         Guest = rt.models.cal.Guest
    #         enrole = ar.rqdata.get('enrole', False)
    #         e = Enrolment.objects.filter(slot=self)
    #         guests = Guest.objects.filter(event=self.cal_entry, partner=calling_user.partner)
    #         if enrole == 'true':
    #             slots = self.__class__.objects.filter(cal_entry=self.cal_entry)
    #             enrolments = Enrolment.objects.filter(
    #                 slot__in=slots,
    #                 pupil=calling_user.person
    #             ).exclude(slot=self)
    #             enrolments.delete()
    #             if e.exists():
    #                 assert e.count() == 1
    #                 e = e[0]
    #             else:
    #                 e = Enrolment()
    #             e.start_date = self.cal_entry.start_date
    #             e.end_date = self.cal_entry.start_date
    #             e.pupil = calling_user.person
    #             e.slot = self
    #             e.full_clean()
    #             e.save()
    #             if not guests.exists():
    #                 g = Guest(event=self.cal_entry, partner=calling_user.person)
    #                 g.full_clean()
    #                 g.save()
    #         elif enrole == 'false':
    #             guests.delete()
    #             if e.exists():
    #                 e.delete()


class Line(Line, RecurrenceSet, EventGenerator, SlotRelatable):

    site_field_name = 'room'

    class Meta(Line.Meta):
        abstract = dd.is_abstract_model(__name__, 'Line')

    cal_entries = GenericRelation('cal.Event',
        content_type_field='owner_type',
        object_id_field='owner_id',
        related_query_name='line')

    slots = GenericRelation('courses.Slot',
        content_type_field='owner_type',
        object_id_field='owner_id',
        related_query_name='line')

    associate_contact_person = dd.ForeignKey(
        dd.plugins.courses.physicians_associate_model,
        on_delete=models.SET_NULL, null=True, blank=True,
        verbose_name=dd.plugins.courses.physicians_associate_label
    )
    associate_contact_role = dd.ForeignKey(
        'contacts.RoleType', on_delete=models.SET_NULL, null=True, blank=True)

    room = dd.ForeignKey('cal.Room')

    case_continued_fee = dd.ForeignKey('products.Product', null=True, blank=True)

    def on_create(self, ar):
        if not self.average_dispatch_time:
            self.average_dispatch_time = dd.plugins.courses.physicians_average_dispatch_time
        return super().on_create(ar)

    def full_clean(self, *args, **kwargs):
        get_appendable_positions_text = lambda position: position if self.positions == "" else " " + position
        self.positions = ""
        if self.monday: self.positions += get_appendable_positions_text("0")
        if self.tuesday: self.positions += get_appendable_positions_text("1")
        if self.wednesday: self.positions += get_appendable_positions_text("2")
        if self.thursday: self.positions += get_appendable_positions_text("3")
        if self.friday: self.positions += get_appendable_positions_text("4")
        if self.saturday: self.positions += get_appendable_positions_text("5")
        if self.sunday: self.positions += get_appendable_positions_text("6")
        if len(self.positions) == 1:
            self.positions = ""
        super().full_clean(*args, **kwargs)

    @dd.virtualfield(dd.PriceField(_("Case enrolment fee")), editable=True)
    def case_enrolment_fee(self, ar, **kwargs):
        if self.fee is not None:
            return self.fee.sales_price

    @dd.virtualfield(dd.PriceField(_("Case continued fee")), editable=True)
    def case_continued_fee_vf(self, ar, **kwargs):
        if self.case_continued_fee is not None:
            return self.case_continued_fee.sales_price

    def get_recurrence_set(self):
        return self

    def update_cal_event_type(self):
        if self.event_type == rt.models.cal.EventType.objects.get(ref='doctors_consultation'):
            return rt.models.cal.EventType.objects.get(ref='availability')
        return self.event_type

    def update_cal_room(self, i):
        return self.room

    def before_auto_event_save(self, event):
        """
        Set room and start_time/end_time for automatic events.
        """
        assert not settings.SITE.loading_from_dump
        assert event.owner == self
        event.summary = 'availability'
        event.has_child_entries = True
        if event.average_dispatch_time is None:
            event.average_dispatch_time = self.average_dispatch_time
        event.child_event_type = rt.models.cal.EventType.objects.get(ref='appointment')
        self.create_or_update_slots()
        super().before_auto_event_save(event)

    def get_author(self):
        return rt.models.users.User.objects.get(
            association=self.company, partner__pk=self.contact_person.pk)

    def before_ui_save(self, ar, cw):
        super().before_ui_save(ar, cw)

        def get_product(fee):
            type = rt.models.products.ProductTypes.appointment
            cat = rt.models.products.Category.objects.get(name="Appointment")
            fee_product = rt.models.products.Product(
                name=_("Appointment"),
                product_type=type,
                category=cat,
                vendor=self.company,
                seller=self.contact_person,
                sales_price=fee
            )
            fee_product.full_clean()
            fee_product.save()
            return fee_product

        def update_fee(fee, name):
            if getattr(self, name) is None:
                setattr(self, name, get_product(fee))
            else:
                fee_obj = getattr(self, name)
                fee_obj.sales_price = fee
                fee_obj.full_clean()
                fee_obj.save()

        fee = ar.rqdata.get('case_enrolment_fee', None)
        if fee is not None:
            update_fee(fee, 'fee')

        fee_continued = ar.rqdata.get('case_continued_fee_vf', None)
        if fee_continued is not None:
            update_fee(fee_continued, 'case_continued_fee')

    def after_ui_save(self, ar, cw):
        super().after_ui_save(ar, cw)
        # self.update_auto_events(ar)

    @classmethod
    def get_pks_for_role_per_company(cls, role, company):
        return [item['person__pk'] for item in rt.models.contacts.Role.objects.filter(
            company=company,
            type=role
        ).values('person__pk')]

    @dd.chooser()
    def associate_contact_person_choices(cls, ar):
        calling_user = ar.get_user()
        return rt.models.contacts.Worker.objects.filter(
            pk__in=cls.get_pks_for_role_per_company(
                rt.models.contacts.RoleType.objects.get(name='Reception clerk'),
                calling_user.association
            )
        )

    @dd.chooser()
    def contact_person_choices(cls, ar):
        calling_user = ar.get_user()
        Worker = dd.resolve_model(dd.plugins.courses.physician_model)
        if calling_user.user_type == UserTypes.staff:
            return Worker.objects.filter(
                pk__in=cls.get_pks_for_role_per_company(
                    rt.models.contacts.RoleType.objects.get(name='Physician'),
                    calling_user.association
                )
            )
        elif calling_user.user_type == UserTypes.physician:
            return Worker.objects.filter(pk__in=[calling_user.worker.pk])
        return Worker.objects.all()

    @dd.chooser()
    def room_choices(cls, ar):
        return rt.models.cal.Room.objects.filter(company=ar.get_user().association)

dd.update_field('courses.Line', 'course_area', default='general_consultation')

class Prescription(Created):
    class Meta:
        app_label = 'courses'
        verbose_name = _("Prescription")
        verbose_name_plural = _("Prescriptions")

    course = dd.ForeignKey('courses.Course')
    diagnosis = dd.RichTextField(_("Diagnosis"))
    prognosis = dd.RichTextField(_("Prognosis"))
    recommendation = dd.RichTextField(_("Recommendation"))
    analysis = dd.RichTextField(_("Require analysis"))
    remarks = dd.RichTextField(_("Remarks"))

    def __str__(self):
        return gettext("prescription #") + str(self.id)


class Symptom(dd.Model):

    class Meta:
        app_label = 'courses'
        verbose_name = _("Symptom")
        verbose_name_plural = _("Symptoms")
        abstract = dd.is_abstract_model(__name__, 'Symptom')

    name = dd.BabelCharField(max_length=80, default="", blank=True)
    description = dd.BabelTextField(default="", blank=True)

    def __str__(self):
        name = dd.babelattr(self, 'name')
        if not name:
            name = self.name
        return name

class SymptomPerPrescription(dd.Model):
    class Meta:
        app_label = 'courses'
        verbose_name = _("Symptom")
        verbose_name_plural = _("Symptoms")
        abstract = dd.is_abstract_model(__name__, 'SymptomPerPrescription')

    prescription = dd.ForeignKey('courses.Prescription')
    symptom = dd.ForeignKey('courses.Symptom')

    @dd.chooser()
    def symptom_choices(self):
        return rt.models.courses.Symptom.objects.all()

    def create_symptom_choice(self, text, ar=None):
        s = Symptom(name=text)
        s.full_clean()
        s.save()
        self.symptom = s

class AdministrationRoute(dd.Model):
    class Meta:
        app_label = 'courses'
        verbose_name = _("Medicine administration route")
        verbose_name_plural = _("Medicine administration routes")

    name = dd.CharField(_("Name"), max_length=80)
    definition = dd.RichTextField(_("Definition"), default="", blank=True)
    short_name = dd.CharField(_("Short name"), max_length=30, default="", blank=True)
    fda_code = dd.CharField(max_length=3, default="", blank=True)
    nci_concept_id = dd.CharField(max_length=30, default="", blank=True)
    local_name = dd.BabelCharField(max_length=80, default="", blank=True)
    description = dd.BabelTextField(default="", blank=True)

    def __str__(self):
        if self.local_name != "":
            return self.local_name
        return self.name


class MedicinePerPrescription(dd.Model):

    class Meta:
        app_label = 'courses'
        verbose_name = _("Medicine")
        verbose_name_plural = _("Medicines")
        unique_together = ('prescription', 'medicine')

    prescription = dd.ForeignKey('courses.Prescription')
    medicine = dd.ForeignKey('products.Medicine')
    ad_freq = models.PositiveSmallIntegerField(_("Administration frequency"), default=1)
    ad_freq_unit = Frequencies.field(default='hourly')

    @dd.chooser()
    def medicine_choices(cls):
        return rt.models.products.Medicine.objects.all()

    def create_medicine_choice(self, text, ar=None):
        med = rt.models.products.Medicine(name=text)
        med.full_clean()
        med.save()
        self.medicine = med


from .ui import *
