# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

import math
import datetime

from lino.api import dd, _

from django.db import models
from django.contrib.contenttypes.fields import ReverseGenericManyToOneDescriptor

class EntryRelatableSlot(dd.Model):
    class Meta:
        abstract = True

    def get_event_kwargs(self):
        return dict(
            start_time = self.start_time,
            end_time = self.end_time,
            sequence = self.seqno
        )

UNDEFINED = 'UNDEFINED'

class SlotRelatable(dd.Model):
    start_time = UNDEFINED
    end_time = UNDEFINED
    slots = UNDEFINED

    @classmethod
    def on_analyze(cls, site):
        if cls.start_time is UNDEFINED or not isinstance(cls.start_time.field, models.TimeField):
            raise Exception(_("A SlotRelatable must implement a start_time field of type django.db.models.TimeField OR lino.api.dd.TimeField!"))
        if cls.end_time is UNDEFINED or not isinstance(cls.end_time.field, models.TimeField):
            raise Exception(_("A SlotRelatable must implement a end_time field of type django.db.models.TimeField OR lino.api.dd.TimeField!"))
        if cls.slots is UNDEFINED or not isinstance(cls.slots, ReverseGenericManyToOneDescriptor):
            raise Exception(_("A SlotRelatable must define a GenericRelation on the name `slots`!"))
        super().on_analyze(site)

    class Meta:
        abstract = True

    average_dispatch_time = dd.DurationField(null=True, blank=True)

    date_time = lambda self, time, date: datetime.datetime(date.year, date.month, date.day, time.hour, time.minute, time.second)

    def get_slot_count(self):
        date = dd.today()
        return math.floor((self.date_time(self.end_time, date) - self.date_time(self.start_time, date)) / self.average_dispatch_time.as_timedelta())

    def create_slots(self):
        date = dd.today()
        slot_count = self.get_slot_count()
        start_time = self.start_time
        td = self.average_dispatch_time.as_timedelta()
        for i in range(1, slot_count + 1):
            start_date_time = self.date_time(start_time, date)
            assert (start_date_time + td).date() == date
            end_time = (start_date_time + td).time()
            assert self.date_time(end_time, date) <= self.date_time(self.end_time, date)
            self.slots.create(
                seqno=i, start_time=start_time, end_time=end_time, owner=self)
            start_time = end_time

    def create_or_update_slots(self):
        date = dd.today()
        td = self.average_dispatch_time.as_timedelta()
        if self.slots.count():
            s1 = self.slots.first()
            if self.date_time(s1.end_time, date) - self.date_time(s1.start_time, date) != td:
                self.slots.all().delete()
                self.create_slots()
        else:
            self.create_slots()

class VariableSlotRelatable(SlotRelatable):
    class Meta:
        abstract = True

    def variable_slots(self):
        return False

    def deletable_overtime_slot(self):
        return False

    def update_remaining(self, slot):
        if slot.owner.variable_slots():
            date = dd.today()
            end_date_time = self.date_time(slot.owner.end_time, date)
            slots = slot.owner.slots
            td = slot.owner.average_dispatch_time.as_timedelta()
            _s = slot
            more = True
            deletable = self.deletable_overtime_slot()
            for s in slots.filter(seqno__gt=slot.seqno):
                if self.date_time(s.end_time, date) > end_date_time:
                    more = False
                    if deletable:
                        to_del = slots.filter(seqno__gte=s.seqno)
                        break
                s.start_time=_s.end_time
                s.end_time=(self.date_time(_s.end_time, date) + td).time()
                s.save()
                _s = s
            if not more:
                to_del.delete()
            else:
                sdt = self.date_time(_s.end_time, date) + td
                for i in range(math.floor((end_date_time - sdt) / td)):
                    edt = sdt + td
                    slots.create(
                        seqno=_s.seqno + i + 1,
                        start_time=sdt.time(),
                        end_time=edt.time(),
                        owner=slot.owner
                    )
                    sdt = edt
