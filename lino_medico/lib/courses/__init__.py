# -*- coding: UTF-8 -*-
# Copyright 2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""Adds functionality for managing courses or other activities.

See :doc:`/specs/courses`.

"""

from lino_xl.lib.courses import *

from lino.api import ad, _


class Plugin(Plugin):
    verbose_name = _("Activities")
    extends_models = ['Line', 'Slot', 'Course', 'Enrolment']

    physician_model = 'contacts.Worker'
    teacher_model = physician_model
    physician_label = _("Physician")
    """The verbose name of pointers to teacher_model. """
    teacher_label = physician_label

    physicians_associate_model = 'contacts.Worker'
    physicians_associate_label = _("Reception clark")
    physicians_average_dispatch_time = '0:15'

    def setup_main_menu(self, site, user_type, main, ar=None):
        m = main.add_menu(self.app_label, self.verbose_name)
        m.add_action('courses.PhysiciansOutdoorLines')
        m.add_action('courses.MyOutdoorLines')
        m.add_action('courses.Prescriptions')
        # for ca in site.models.courses.ActivityLayouts.objects():
        #     m.add_action(ca.courses_table)
        # m.add_separator()
        # m.add_action('courses.Lines')
        # m.add_action('courses.PendingRequestedEnrolments')
        # m.add_action('courses.PendingConfirmedEnrolments')
        # m.add_action('courses.MyCoursesGiven')

    def setup_config_menu(self, site, user_type, m, ar=None):
        m = m.add_menu(self.app_label, self.verbose_name)
        # m.add_action('courses.Topics')
        # m.add_action('courses.Slots')

    def setup_explorer_menu(self, site, user_type, m, ar=None):
        m = m.add_menu(self.app_label, self.verbose_name)
        # m.add_action('courses.AllActivities')
        # m.add_action('courses.AllEnrolments')
        # m.add_action('courses.EnrolmentStates')
        # m.add_action('courses.ActivityLayouts')
        # m.add_action('courses.CourseStates')

    def get_dashboard_items(self, user):
        for x in super(Plugin, self).get_dashboard_items(user):
            yield x
        if user.is_authenticated:
            yield self.site.models.courses.MyCoursesGiven
            yield self.site.models.courses.MyActivities
        yield self.site.models.courses.StatusReport
