from lino.api import dd, rt, _

def skill(designation):
    return rt.models.skills.Skill(**dd.str2kw('name', designation))

def group(designation):
    return rt.models.groups.Group(**dd.str2kw('name', designation))

def objects():
    yield group(_("Hospital A Department 1"))
    yield group(_("Hospital B Department 1"))
    yield group(_("Hospital B Department 2"))
