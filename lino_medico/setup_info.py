# -*- coding: UTF-8 -*-
# Copyright 2021-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

SETUP_INFO = dict(
    name='lino-medico',
    version='0.2.2-alpha',
    install_requires=['lino-xl'],
    description=("A software ecosystem for record keeping of and managing nationwide health care facilities."),
    author='Rumma & Ko Ltd',
    author_email='info@lino-framework.org',
    url="https://gitlab.com/lino-framework/medico",
    license_files=['COPYING'],
    test_suite='tests')

SETUP_INFO.update(long_description="""
A hospital management application (in development);

Scope
=====

A global database system for all indexed hospital

  - Hospital's internal access for management
  - External access when needed

Users (patients) can access personal medical history via web portal
""")

SETUP_INFO.update(classifiers="""
Programming Language :: Python
Programming Language :: Python :: 3
Development Status :: 1 - Beta
Environment :: Web Environment
Framework :: Django
Intended Audience :: Health Care Facilitators
Intended Audience :: Health Care Users
Intended Audience :: Health Care Cogs
Intended Audience :: Health Care Researchers
License :: OSI Approved :: GNU Affero General Public License v3
Operating System :: OS Independent
Topic :: Health Care
""".format(**SETUP_INFO).strip().splitlines())
SETUP_INFO.update(packages=[
    'lino_medico',
    'lino_medico.lib',
    'lino_medico.lib.cal',
    'lino_medico.lib.cal.fixtures',
    'lino_medico.lib.contacts',
    'lino_medico.lib.contacts.fixtures',
    'lino_medico.lib.courses',
    'lino_medico.lib.courses.fixtures',
    'lino_medico.lib.households',
    'lino_medico.lib.households.fixtures',
    'lino_medico.lib.lets',
    'lino_medico.lib.medico',
    'lino_medico.lib.medico.fixtures',
    'lino_medico.lib.products',
    'lino_medico.lib.products.fixtures',
    'lino_medico.lib.reception',
    'lino_medico.lib.reception.fixtures',
    'lino_medico.lib.users',
    'lino_medico.lib.users.fixtures',
    'lino_medico.projects',
    'lino_medico.projects.medico1',
    'lino_medico.projects.medico1.tests',
    'lino_medico.projects.medico1.settings',
    'lino_medico.projects.medico1.settings.fixtures',
])

SETUP_INFO.update(package_data=dict())
