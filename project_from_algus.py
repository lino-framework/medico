import os
import shutil
import re
from datetime import datetime

hist = dict(dir_changed=0, f_changed = 0, fs = 0, edited = 0)

def rename_dir(name, dr='.', hist=hist):
    for root, dirs, files in os.walk(dr):

        for d in dirs:
            if d == '__pycache__':
                shutil.rmtree(os.path.join(root, d))
                continue
            if d == '.git':
                continue
            new_d = None
            if 'algus' in d:
                new_d = os.path.join(root, d.replace('algus', name))
            elif 'Algus' in d:
                new_d = os.path.join(root, d.replace('algus', name.capitalize()))
            if new_d:
                hist['dir_changed'] += 1
                shutil.move(
                    os.path.join(root, d), new_d, copy_function=shutil.copytree)
                d = new_d
            else:
                d = os.path.join(root, d)
            rename_dir(name, d, hist)

        for f in files:
            if f == __file__ or (not f.endswith('.py') and not f.endswith('.rst') and not f.endswith('.html')):
                continue
            hist['fs'] += 1
            new_f = None
            if 'algus' in f:
                new_f = os.path.join(root, f.replace('algus', name))
            elif 'Algus' in f:
                new_f = os.path.join(root, f.replace('Algus', name.capitalize()))
            if new_f:
                hist['f_changed'] += 1
                shutil.move(os.path.join(root, f), new_f, copy_function=shutil.copy2)
                f = new_f
            else:
                f = os.path.join(root, f)
            cr = 'Copyright ' + str(datetime.today().year) + ' Rumma'
            with open(f, 'r+') as f:
                try:
                    c = f.read()
                    content = c.replace('algus', name).replace('Algus', name.capitalize())
                    content = re.sub(r'Copyright [0-9]{4} Rumma', cr, content)
                    content = re.sub(r'Copyright [0-9]{4}-[0-9]{4} Rumma', cr, content)
                    f.seek(0)
                    f.write(content)
                    f.truncate()
                    if content != c:
                        hist['edited'] += 1
                except UnicodeDecodeError as e:
                    print(f, e)

if __name__=='__main__':
    import sys
    if len(sys.argv) > 1:
        p_name = sys.argv[1]
    else:
        p_name = 'medico'
    print(f"Creating project lino_{p_name} from lino_algus...")
    rename_dir(p_name)
    print(f"Renamed {hist['dir_changed']} directories and {hist['f_changed']} files.")
    print(f"Found {hist['fs']} files and edited {hist['edited']} files.")
